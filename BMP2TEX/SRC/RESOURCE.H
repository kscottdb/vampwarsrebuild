//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BMP2TEX.rc
//
#define IDD_DIALOG_SPEC                 101
#define IDC_RADIO_SINGLE                1000
#define IDC_RADIO_SERIES                1001
#define IDC_EDIT_FROM                   1002
#define IDC_STATIC_FROM                 1003
#define IDC_STATIC_TO                   1004
#define IDC_EDIT_TO                     1005
#define IDC_CHECK_TRIM                  1006
#define IDC_BUTTON_CHANGE               1007
#define IDC_STATIC_HORZ                 1008
#define IDC_STATIC_ORIGIN               1010
#define IDC_RADIO_HLEFT                 1011
#define IDC_RADIO_HCENTER               1012
#define IDC_RADIO_HRIGHT                1013
#define IDC_RADIO_HEXACT                1014
#define IDC_EDIT_HEXACT                 1015
#define IDC_STATIC_VERTICAL             1016
#define IDC_RADIO_VTOP                  1017
#define IDC_RADIO_VCENTER               1018
#define IDC_RADIO_VBOTTOM               1019
#define IDC_RADIO_VEXACT                1020
#define IDC_EDIT_VEXACT                 1021
#define IDC_STATIC_RELATIVE             1022
#define IDC_RADIO_PRE_TRIM              1023
#define IDC_RADIO_POST_TRIM             1024
#define IDC_STATIC_PREVIEW              1025
#define IDC_CHECK_OLD                   1026
#define IDC_CHECK_LOOPS                 1027
#define IDC_EDIT_LOOPS                  1028
#define IDC_STATIC_VERSION              1029
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
