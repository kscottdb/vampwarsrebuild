// INVPLANE.H -- the inventory plane for single or dual portraits
//
// TW

#ifndef _invplane
#define _invplane

#include "sciplane.h"
#include "sciprop.h"

class Unit;
class Item;
class StatusLine;

class InventoryPlaneItem : public sciProp
{
public:
	InventoryPlaneItem();
	~InventoryPlaneItem();
	Boolean HandleEvent(sciEvent & oEvent);
	void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
	void vSetStatusText(const char *pstrNewText);
	void Render (RECT &rUpdateRect);
	Boolean bOnMe(int nX, int nY);
	void Hide();
	void Show(Item *poItem);
	void vSetRect(int nL, int nT, int nR, int nB) {SetRect(&moRect, nL, nT, nR, nB);}
	void Posn(int nX, int nY);
	int nLeft() {return moRect.left;}
	int nTop() {return moRect.top;}

protected:
	RECT		moRect;			//the rectangle bOnMe uses for its check
	StatusLine	*mpoStatusLine;
	char		*mpoStatusText;	//the text that will be display on the status line when mouse is over it{moItem1.vSetRect(int nL, int nT, int nR, int nB);}
	Item		*mpoItem;		//backpointer to the item we're displaying for

};


class InventoryPlane : public sciPlane {
	
public:
	InventoryPlane();
	~InventoryPlane();
	void Doit();
	void Render (RECT &rUpdateRect);
	Boolean HandleEvent(sciEvent & oEvent);
	void Show(Unit *poUnit);
	void vShowItems();
	void Hide();
	void Posn(int nX, int nY, BOOL bUpdateWindow = TRUE);
	Unit *poUnit() {return mpoUnit;}
	void vSetStatusLine(StatusLine *poStatusLine);
	void vSetRect1(int nL, int nT, int nR, int nB) {moItem1.vSetRect(nL, nT, nR, nB);}
	void vSetRect2(int nL, int nT, int nR, int nB) {moItem2.vSetRect(nL, nT, nR, nB);}
	void vSetRect3(int nL, int nT, int nR, int nB) {moItem3.vSetRect(nL, nT, nR, nB);}
	int nNumItems() {return mnNumItems;}
protected:
	Unit				*mpoUnit;	//a backpointer to the unit we're drawing the inventory items for

	InventoryPlaneItem	moItem1;
	InventoryPlaneItem	moItem2;
	InventoryPlaneItem	moItem3;

	int					mnNumItems;
};


#endif
