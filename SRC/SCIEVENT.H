#ifndef _scievent
#define _scievent

#include "sciobj.h"
#include "os.h"

class sciPlane;

typedef enum
	{
	SHIFT_DOWN	= 0x0001,
	CTRL_DOWN	= 0x0002,
	ALT_DOWN	= 0x0004,
	LEFT_MOUSE	= 0x0008,
	RIGHT_MOUSE	= 0x0010,
	} EVENT_MODIFIER;

typedef enum
	{
	MOUSE_DOWN,
	MOUSE_UP,
	MOUSE_MOVE,
	KEY_PRESS,
	KEY_CHAR,
	} EVENT_TYPE;

class sciEvent : public sciObject
	{
	public:
		sciEvent();
		~sciEvent();

		void vLocalize(sciPlane *poPlane);
		void vGlobalize(void);

		Boolean claimed;
		int x;
		int y;
		char c;
		EVENT_TYPE type;
		int modifiers;
		sciPlane *plane;

	protected:

	private:
	};

#endif //_scievent

