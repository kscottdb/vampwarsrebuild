#ifndef _HUMANVAMPIREHUNTER_H_
#define _HUMANVAMPIREHUNTER_H_

// Vampire Hunters
//		ruthlessly hunt and destroy vampires
//		detect vampires at a distance
//		disarm vampire traps
//		fight as well as vampires hand to hand
//		use their own spells
//		raise anxiety level of the populace
//		cannot be converted

class HumanVampireHunter : public Human
{
	friend class Unit;
public:
	int vGetVisibleRange();

protected:
	HumanVampireHunter(Player *poOwner, MapPlane *poPlane, MapNode *poNode, int nTemplateID);

	// OVERRIDES
	void vUpdateAttributes();
	void vSetLevel(int nLevel);
	void vThink();
	void vFormAMobIfNecessary();
	void vLeaveMobIfNecessary();
	BOOL bCanKillVampireLord() { return TRUE; }
	/*
	vScan
		vCommand
		vCast
		vThink
		bCanCast()
		bCanBeConverted()
		vCreate*/
};

#endif

