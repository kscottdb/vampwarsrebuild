#ifndef _H_palette_
#define _H_palette_

#include "vpalette.h"

class Palette : public VPalette
	{
	public:
		Palette(int resourceID);
		~Palette();
		void ActivatePalette();
		HPALETTE ActivatePalette(HDC hDC);

	private:
		HPALETTE hpalTheOneTruePalette;
		HPALETTE hpalOldSave;
	};

class Palette16 
{
public:
	Palette16();
	Palette16(const VPalette *poVPal);
	Palette16(RGBQUAD *pRGBQuads);
	Palette16(Palette16& oP);
	~Palette16();

	void vConvert(const VPalette *poVPal);
	void vConvert(RGBQUAD *pRGBQuads);
	
	BOOL bIsFull(void) {return mpwPalette != NULL;}

	WORD wGetColor(BYTE jRef) {
		return mpwPalette[jRef];
	}

	WORD *pwGetPalette(void) {return mpwPalette;}
	void vGetCopy(WORD *pwPalette);

	void vSetColor(BYTE jRef, WORD wColor);
	void vSetColor(BYTE jRef, RGBQUAD *pRGBQuad);
	void vSetColor(BYTE jRef, BYTE jRed, BYTE jGreen, BYTE jBlue);

	Palette16& operator=(Palette16& oP);
	operator WORD *() {return mpwPalette;}

protected:
	void vInit(void);
	void vAllocate(void);

	WORD *mpwPalette;
};

#endif
