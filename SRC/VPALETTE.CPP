#include "os.h"

#include "vpalette.h"
#include "app.h"
#include "window.h"
#include "texture.h"
#include <math.h>

extern Application *pApp;

static VPalette *lastPalette = NULL;

#define DEBUG_QUICKSTART_NOCALCULATEALPHA

VPalette::VPalette(int resourceID)
	: Resource(RES_PALETTE, resourceID, NULL)
	{
	}

VPalette::~VPalette()
	{
	if (lastPalette == this)
		lastPalette = NULL;
	}

void VPalette::ActivatePalette()
	{
	if (lastPalette != this)
		{
		pApp->pWindow->Erase(pApp->pWindow->rcInner);
		lastPalette = this;
		CalculateAlpha();
		}
	}

void VPalette::CalculateAlpha()
	{
#ifndef DEBUG_QUICKSTART_NOCALCULATEALPHA
	int f, b;

	for (f = 0; f < 256; ++f)	// foreground color
		{
		for (b = 0; b < 256; ++b)	// background color
			{
			alpha[f][b] = PaletteCalculateAlpha(colors, f, b, 150);
			}
		}
#endif
	}
