// PORTRAIT.H -- A  portrait of a selected unit
//
// TW

#ifndef _portrait
#define _portrait

#include "sciprop.h"
#include "scitext.h"
#include "bargraph.h"

class sciPlane;
class Unit;
class BarGraph;
class StatusLine;
class InventoryPlane;
class Template;

typedef enum _PORTRAIT_UNIT_TYPE_ {
	PT_LOCAL_VAMPIRE,
	PT_ENEMY_VAMPIRE,
	PT_PEASANT,
} PORTRAIT_UNIT_TYPE;


// The portraits used when there are three or more local vampire selected or
// one or more of any non-local vampires selected
class Portrait : public sciProp {
public:
	Portrait(sciPlane *poPlane, Template *poTemplate);
	~Portrait();
	virtual void vSetUp();
	void Posn(int posnX, int posnY);
	void Render(RECT &updateRect);
	void Show(Unit *poUnit);
	void Hide();
	void Doit();
	void SetPri(int newPri);	
	virtual void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
	Boolean HandleEvent(sciEvent &oEvent);
	virtual Boolean bHeadHandleEvent(sciEvent &oEvent);
	Unit *poAssociatedUnit() {return mpoUnit;}
	void vSetStatusText(const char *pstrNewText);
	static Unit *mpoUnitToSummarize;
protected:
	Template	*mpoTemplate;		//pointer to the clan's interface template
	Unit		*mpoUnit;			//a backpointer to the unit we're drawing the Portrait for
	sciText		*mpoLevel;
	sciText		*mpoCloak;			//'cloaked' indicator (actually displays if 'uncloaked')
	BarGraph	*mpoPower;
	BarGraph	*mpoHealth;
	BarGraph	*mpoUpgrade;
	Boolean		mbDrawUpgrade;		//T when upgrade needs to be drawn(upgrade in progress)
	StatusLine	*mpoStatusLine;
	int			mnCurUnitLevel;		//the level mpoUnit is currently set at
	void		vSetLevel();		//sets the text level indicator
	Boolean		mbCloaked;
	PORTRAIT_UNIT_TYPE meUnitType;	//local vampire, enemy vampire or peasant type (slightly different displays for each)
private:
	int			mnLocalPlayer;		//local player number
	char		*mpoStatusText;		//the text 'name' that will be display on the status line when mouse is over it
};


// The portrait used when there is only one local vampire selected
class SinglePortrait : public Portrait {
public:
	SinglePortrait(sciPlane *poPlane, Template *poTemplate);
	~SinglePortrait();
	void SetPri(int newPri);	//make sure to call SetPri AFTER vSetUp
	void Posn(int posnX, int posnY);
	void Render(RECT &updateRect);
	Boolean bHeadHandleEvent(sciEvent &oEvent);
	void vSetUp();
	void Show(Unit *poUnit);
	void Hide();
	void vSetStatusLine(StatusLine *poStatusLine);
	void vRefreshInventory();
protected:
	sciView			*mpoHead;
	InventoryPlane	*mpoInvPlane;
};

// The portrait used when there are only two local vampires selected
class DualPortrait : public Portrait {
public:
	DualPortrait(sciPlane *poPlane, Template *poTemplate);
	~DualPortrait();
	void SetPri(int newPri);	//make sure to call SetPri AFTER vSetUp
	void Posn(int posnX, int posnY);
	void Render(RECT &updateRect);
	Boolean bHeadHandleEvent(sciEvent &oEvent);
	void vSetUp();
	void Show(Unit *poUnit);
	void Hide();
	void vSetStatusLine(StatusLine *poStatusLine);
	void vRefreshInventory();
	int nInventorySize(); 
protected:
	sciView			*mpoHead;
	InventoryPlane	*mpoInvPlane;
};



#endif