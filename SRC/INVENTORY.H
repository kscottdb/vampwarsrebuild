#ifndef _INVENTORY_H_
#define _INVENTORY_H_

class HasInventory 
{
public:
	HasInventory() {}
	~HasInventory() {}

	void Drop(MapNode * poWhere, Item * poWhat);
	void DropAll(MapNode *poWhere);
	void Take(MapNode * poWhere, Item * poWhat);
	void Give(HasInventory * pWho, Item * poWhat);

	RefList<Item> *poItemList() {return &moItems;}
	int nIndex(Item *poItem) {return moItems.Find((Item*)poItem);}

protected:
	RefList<Item> moItems;
};

#endif