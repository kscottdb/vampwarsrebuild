#ifndef _NONPLAYERCLASS_H_
#define _NONPLAYERCLASS_H_

typedef enum TAG_ENUM_THRALLACTION
{
	THRALLACTION_NOTHINGHAPPENS,
	THRALLACTION_ISUNDERCOMMAND,
	THRALLACTION_UNITSLOWSDOWN,
} ENUM_THRALLACTION;

class vClan;
class Vampire;

class NonPlayerClass : public vClan {
public:
	NonPlayerClass(Template & oNpcTemplate);

	// Default Upgrade Settings
	int GetUpgradeStealth();
	int GetUpgradeDetection();
	int GetUpgradeAttack();
	int GetUpgradeDefend();
	int GetUpgradeCasting();
	int GetUpgradeSpeed();
	int GetUpgradeSight();
	int GetUpgradeLevel();

	BOOL bCastsSpells();
	BOOL bConvertibleByBiting();

	ENUM_THRALLACTION eWhenThralled();

	// Default Converted Vampire Settings
	int GetVampireStealth();
	int GetVampireDetection();
	int GetVampireAttack();
	int GetVampireDefend();
	int GetVampireCasting();
	int GetVampireSpeed();
	int GetVampireSight();
	int GetVampireLevel();

	HRESULT bGetFirstSpell(int * pnSpellId);
	HRESULT bGetNextSpell(int * pnSpellId);
	HRESULT vCreateConvert(Vampire ** ppoVampire, Vampire * poParentVampire, Unit * poOriginalUnit, MapNode * poNode, MapPlane * poPlane);

	int nGetMaxAnxiety();
	int nGetAnxietyEnterMob();
	int nGetAnxietyLeaveMob();
	int GetAnxietyIfInvitedToJoinMob();
	float fGetAnxietyFalloffRate();

protected:
	int mnSpell;
	float mfAnxietyFalloffRate;
};


#endif