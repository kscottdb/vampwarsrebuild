#include "scimain.h"
#include "mapnode.h"
#include "mapplane.h"
#include "unit.h"
#include "building.h"
#include "command.h"
#include "objectdb.h"
#include "path.h"
#include "template.h"
#include "mapedit.h"
#include "guard.h"
#include "movement.h"
#include "procs.h"
#include "math.h"
#include "intplane.h"
#include "vampire.h"
#include "item.h"
#include "mob.h"
#include "NonPlayerClass.h"

int gnMobCount = 0;

MobCoordinator::~MobCoordinator()
{
	gnMobCount--;
}

Unit * MobCoordinator::GetUnitToFollow()
{
	int nIndex = moFollowers.GetSize() + 1;
	nIndex = GlobalRand((int)((double)nIndex/2.6));
	Unit * poUnit = NULL;
	if (nIndex == 0)
	{
		poUnit = mpoLeader;
	}
	else
	{
		nIndex--;
		poUnit = moFollowers.GetItem(nIndex);
	}
	return poUnit;
}

void MobCoordinator::Regroup()
{
	/*
	RefList<Unit> moStillAlive;
	int nCount = moFollowers.GetSize();
	Unit *poUnit;
	while (nCount)
	{
		nCount--;
		poUnit = moFollowers.GetItem(nCount);
		if (poUnit->bAlive())
		{
			moStillAlive.AddToEnd(poUnit);
		}
	}
	moFollowers.RemoveAll();
	nCount = moStillAlive.GetSize();
	Command oCommand;
	oCommand.meType = CM_MOVE_FOLLOW;
	oCommand.mbTargetUnit = TRUE;
	while (nCount)
	{
		nCount--;
		poUnit = moStillAlive.GetItem(nCount);
		oCommand.mnUnit = GetUnitToFollow()->nGetID();
		moFollowers.AddToEnd(poUnit);
		poUnit->vCommand(oCommand, TRUE);
	}
	*/
	RefList<Unit> moStillAlive;
	int nCount = moFollowers.GetSize();
	Unit *poUnit;
	while (nCount)
	{
		nCount--;
		poUnit = moFollowers.GetItem(nCount);
		if (poUnit->bAlive())
		{
			moStillAlive.AddToEnd(poUnit);
		}
	}
	moFollowers.RemoveAll();
	nCount = moStillAlive.GetSize();
	Command oCommand;
	oCommand.meType = CM_MOVE_FOLLOW;
	oCommand.mbTargetUnit = TRUE;
	oCommand.mnUnit = mpoLeader->nGetID();
	oCommand.mnNewValue = 0;
	while (nCount)
	{
		nCount--;
		poUnit = moStillAlive.GetItem(nCount);
		moFollowers.AddToEnd(poUnit);
		poUnit->vCommand(oCommand, TRUE);
	}
	mpoLeader->Sequence_Clear();
	mpoLeader->Sequence_WanderMethodically(0);
}

void MobCoordinator::GameCycle()
{
	if (mnLastPlayCycle == 0)
	{
		if (!mnSound && moFollowers.GetSize() > 3)
		{
			mnLastPlayCycle = 60;
			mnSound = 1;
			pApp->pSoundManager->EmitFromObject(this, SOUND_MobLoop1, SOUNDPRIORITY_AMBIENTSOUNDS, TRUE, 128);
		}
		else if (mnSound)
		{
			mnLastPlayCycle = 60;
			vPlayRandomSound(TAG_Soks);
		}
	}
	else
	{
		if (mnSound)
		{
			if (moFollowers.GetSize() <= 3)
			{
				pApp->pSoundManager->SoundEmitterDestroyed(this);
				mnSound = 0;
				mnLastPlayCycle = 0;
				return;
			}
		}
		mnLastPlayCycle--;
	}
}

void MobCoordinator::Disperse()
{
	if (mpoLeader)
	{
		mpoLeader->vSetMobbing(FALSE);
		mpoLeader->vSetAnxiety(0);
		mpoLeader->vCommandNone();
		mpoLeader->Sequence_Clear();
		mpoLeader->Sequence_WanderMethodically();
	}
	int nSize = moFollowers.GetSize();
	while(nSize)
	{
		nSize--;
		Unit * poUnit = moFollowers.GetItem(nSize);
		poUnit->vSetMobbing(FALSE);
		poUnit->vSetAnxiety(0);
		poUnit->vCommandNone();
		poUnit->Sequence_Clear();
		poUnit->Sequence_WanderMethodically();
	}
	delete this;
}

void MobCoordinator::vPlayRandomSound(long lTag)
{
	long *plList;
	long lSize;

	plList = game->mpoTemplate->plGetList(lTag, &lSize);

	if (lSize && plList) 
	{
		int nWhich = random((short)lSize);
		int nSound = (int)plList[nWhich];
		UINT unX,unY;
		GetPlayPosition(&unX, &unY);
		pApp->pSoundManager->EmitFromPosition(unX, unY, nSound, SOUNDPRIORITY_UNITSOUNDS, FALSE, 128);
	}
}

void MobCoordinator::AddUnitAsFollower(Unit * poUnit, Unit * poFollowThisUnit)
{
	vDebugOut("ADD NEW FOLLOWER/n");
	assert(poUnit);
	assert(ID_NONE == moFollowers.Find(poUnit));
	assert(poUnit != mpoLeader);
	moFollowers.AddToEnd(poUnit);
	Command oCommand;
	oCommand.meType = CM_MOVE_FOLLOW;
	oCommand.mbTargetUnit = TRUE;
	oCommand.mnNewValue = 1;
	if (0)
	{
		oCommand.mnUnit = poFollowThisUnit->nGetID();
	}
	else
	{
		oCommand.mnUnit = mpoLeader->nGetID();
	}
	poUnit->Sequence_Clear();
	poUnit->vCommand(oCommand,TRUE);
	poUnit->vSetMobbing(TRUE, this);
	int nAnxiety = ((NonPlayerClass*)poUnit->vGetClan())->GetAnxietyIfInvitedToJoinMob();
	if (nAnxiety > poUnit->GetAnxiety())	
	{
		poUnit->vSetAnxiety(nAnxiety);
	}
}

void MobCoordinator::AlertLeaderOfVampireLocation(Unit * poUnit)
{
	vDebugOut("ALERT LEADER/n");
	assert(poUnit);
	assert(poUnit->bIsVampire());
	// APPROACH TO VERIFY
	if (poUnit->bIsCloaked())
	{
		if (mState == 0)
		{
			// ATTACK
			pApp->pSoundManager->EmitFromObject(poUnit, SOUND_WhoAreYou, SOUNDPRIORITY_AMBIENTSOUNDS);
			mState = 1; // CHECKING
		}

		MapNode * poNode = poUnit->poNode();
		Command oCommand;
		oCommand.meType = CM_MOVE;
		oCommand.mnX = poNode->mnXPos;
		oCommand.mnY = poNode->mnYPos;
		mpoLeader->vCommand(oCommand, TRUE);
	}
	else
	{
		AttackUnit(poUnit);
	}
}

void MobCoordinator::ChooseNewLeader()
{
	vDebugOut("CHOOSE NEW LEADER/n");
	if (moFollowers.GetSize() == 0)
	{
		return;
	}
	Unit * poBestUnit = NULL;
	//int nIndexOfMaxAnxiety = -1; 
	int nMaxAnxiety = 0;
	Unit * poUnit = NULL;
	if (mpoLeader)
	{
		if (mpoLeader->bAlive())
		{
			moFollowers.AddToEnd(mpoLeader);
		}
	}
	int nCount = moFollowers.GetSize();
	int maxscore = -1;
	while (nCount)
	{
		nCount--;
		poUnit = moFollowers.GetItem(nCount);
		if (poUnit->bAlive())
		{
			int score = 0;
			int sx = max(0, poUnit->nGetX()-1);
			int sy = max(0, poUnit->nGetY()-1);
			int ex = min(gpoMap->nGetWidth()-1, poUnit->nGetX()+1);
			int ey = min(gpoMap->nGetHeight()-1, poUnit->nGetY()+1);
			for (int i=sx;i<ex;i++)
				for(int j=sy;j<ey;j++)
				{
					if (!gpoMap->poGetNode(i,j)->bBlocked())
					{
						score++;
					}
				}
			if (score>maxscore)
			{
				maxscore = score;
				poBestUnit = poUnit;
			}
		}
		else
		{
			moFollowers.RemoveIndex(nCount);
		}
	}
	if (poBestUnit)
	{
		mpoLeader = poBestUnit;
		moFollowers.Remove(poBestUnit);
		mpoLeader->vCommandNone();
		mpoLeader->Sequence_Clear();
		mpoLeader->Sequence_WanderMethodically(0);
	}
}

void MobCoordinator::GetPlayPosition(unsigned int * unX, unsigned int * unY)
{
	*unX = (unsigned int)mpoLeader->nPixelX();
	*unY = (unsigned int)mpoLeader->nPixelY();
}