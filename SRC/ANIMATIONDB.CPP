// ANIMATIONDB.CPP -- the cyler objects database
//
// TW

#include "animationdb.h"
#include "cycler.h"

AnimationDB goAnimationDB;

AnimationDB::AnimationDB()
{
	mnSize = 0;
}

void AnimationDB::vAddObject(Cycler *poObject)
{
	assert(poObject);
	moObjects.AddToEnd(poObject);
	mnSize = moObjects.GetSize();
}


void AnimationDB::vRemoveObject(Cycler *poObject)
{
	assert(poObject);
	moObjects.Remove(poObject);
	mnSize = moObjects.GetSize();
}
	
void AnimationDB::vAnimationCycle()
{
	int i;

	for (i = 0; i < mnSize; ++i)
	{
		Cycler *poCycler = moObjects.GetItem(i);
		assert(poCycler);
		poCycler->vAnimationCycle();
	}
}