#ifndef _scibuttn
#define _scibuttn

#include "sciprop.h"
#include "scitext.h"

class sciButton : public sciProp
	{
	public:
		sciButton();
		~sciButton();
		void Init(int newWidth = 0, int newHeight = 0);
		Boolean HandleEvent(sciEvent &event);
		void Enable();
		void Disable();
		Boolean IsEnabled();
		virtual void DoLeftClick();
		virtual void DoRightClick();
		void Render(RECT &updateRect);
		void SetHotkey(char c);
		void SetHotkey2(char c);
		void SetRightHotkey(char c);
		void SetButtonCels(int newNormal, int newDepressed, int newDisabled, int newHilighted);
		void SetButtonDefaultCels();
		void SetRepeat(int newStartDelay = -1, int newRepeatDelay = -1);
		friend class sciButtonRepeatScript;
		void SetText(char *newText);
		void vSetTextSize() {if (text) text->vSetSize();}
		int nTextHeight() {return text ? text->nHeight() : 0;}
		int nTextWidth() {return text ? text->nWidth() : 0;}
		void SetFont(int newNormalFont, int newDepressedFont = -1, int newHiFont = -1, int newDisabledFont = -1); 
		void ToggleFont(Boolean bHilighted);
		void SetView(int newView);
		void SetPri(int newPri);
		void Posn(int x, int y);
		void TextPosn(int addX, int addY);
		void SetTextJust(int newJust);
		void Show();
		void Hide();
	
	protected:
		void SetState(int newState);
		virtual void UpdateCel();

		sciText *text;
		Boolean mouseDown;
		Boolean enabled;
		char hotkey, hotkey2;
		char rightHotkey;
		int justification;
		int normal;
		int depressed;
		int disabled;
		int hilighted;
		int state;
		int startDelay;
		int repeatDelay;
		int normalFont;
		int hilightedFont;
		int disabledFont;
		int depressedFont;

		static sciButton *mpoCurrentHilight;	//current button that is highlighted
	};

enum
	{
	DEFAULT_BUTTON_NORMAL = 0,
	DEFAULT_BUTTON_DEPRESSED,
	DEFAULT_BUTTON_DISABLED,
	DEFAULT_BUTTON_HILIGHTED,
	DEFAULT_BUTTON_NORMAL_SELECTED,
	DEFAULT_BUTTON_DEPRESSED_SELECTED,
	DEFAULT_BUTTON_DISABLED_SELECTED,
	DEFAULT_BUTTON_HILIGHTED_SELECTED,
	DEFAULT_BUTTON_PENDING 
};



#endif // _scibuttn
