#include "os.h"

#include "prf.h"
#include "unihand.h"
#include "symbol.h"
#include "util.h"

#include <string.h>
#include <io.h>

DWORD filelength(FILE *pFile) {
	DWORD len = _filelength(_fileno(pFile));
	return len;
	}

void fcopy(FILE *pDestFile, FILE *pSourceFile, DWORD dwLength) {
	BYTE *pBytes;
	DWORD dwBytesToCopy = dwLength;
	int iBufferSize = 16384;

	pBytes = new BYTE[iBufferSize];
	while (dwBytesToCopy) {
		fread(pBytes, (int)min((DWORD)iBufferSize, dwBytesToCopy), 1, pSourceFile);
		fwrite(pBytes, (int)min((DWORD)iBufferSize, dwBytesToCopy), 1, pDestFile);
		if (dwBytesToCopy > (DWORD)iBufferSize)
			dwBytesToCopy -= iBufferSize;
		else
			dwBytesToCopy = 0;
		}
	delete pBytes;
	}

PRF::PRF() {
	pFile = NULL;
	pPRFFileHeader = NULL;
	pPRFResourceDirectory = NULL;
	fSymbolTable = NULL;
	}

PRF::~PRF() {
	if (pFile)
		fclose(pFile);
	if (pPRFFileHeader)
		delete pPRFFileHeader;
	if (pPRFResourceDirectory)
		delete[] pPRFResourceDirectory;
	}

void PRF::New(char *szFilename) {
	pFile = fopen(szFilename, "wb");
	pPRFFileHeader = new PRF_FILEHEADER;
	
	
	pPRFFileHeader->wByteOrder = 1;
	pPRFFileHeader->c4ID = MAKEC4ID('P','R','F','\0');

	pPRFFileHeader->dwVersion = 1;
	pPRFFileHeader->dwSizeOfResourceDirectory = sizeof(PRF_RESOURCEDIRECTORY) + sizeof(PRF_RESOURCEMAP);
	pPRFFileHeader->dwFileSize = pPRFFileHeader->dwSizeOfResourceDirectory + sizeof(PRF_FILEHEADER);
	pPRFFileHeader->dwResourceDirectoryOffset = sizeof(PRF_FILEHEADER);

	pPRFResourceDirectory = (PRF_RESOURCEDIRECTORY*)new BYTE[pPRFFileHeader->dwSizeOfResourceDirectory];
	pPRFResourceDirectory->wNumOfResources = 1;
	pPRFResourceDirectory->bIncludeDescriptions = TRUE;
	
	// Dummy resource.
	pPRFResourceDirectory->prfResourceMap[0].c4Type = 0;
	pPRFResourceDirectory->prfResourceMap[0].wID = 0;
	pPRFResourceDirectory->prfResourceMap[0].dwOffset = sizeof(PRF_FILEHEADER);
	pPRFResourceDirectory->prfResourceMap[0].dwLength = 0;
	
	WriteHeaders();
	}

void PRF::WriteHeaders() {
	fwrite(pPRFFileHeader, sizeof(PRF_FILEHEADER), 1, pFile);
	fwrite(pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory), 1, pFile);
	}

void PRF::GetFileInfo() {
	pPRFFileHeader = new PRF_FILEHEADER;
	fread(pPRFFileHeader, sizeof(PRF_FILEHEADER), 1, pFile);
	WORD bNeedToSwap = pPRFFileHeader->wByteOrder != 1;
	if(bNeedToSwap)
		SwapPrfFileHeader();
	fseek(pFile, pPRFFileHeader->dwResourceDirectoryOffset, SEEK_SET);
	pPRFResourceDirectory = (PRF_RESOURCEDIRECTORY*)new BYTE[pPRFFileHeader->dwSizeOfResourceDirectory];
	fread(pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory), 1, pFile);

	if(bNeedToSwap) {
		SwapPrfResourceDirectory();

		fSymbolTable = new SymbolTable(1);
		
		//fseek(pFile, beginning of symbol table resource);
		//��� Make sure the symbol table exists
		CHAR4 c4ResourceType = MAKEC4ID('S','Y','M','B');
		DWORD dwResourceNumber = GetResourceIndex(c4ResourceType, 1);
		if( dwResourceNumber == -1 )
			DebugString("Symbol Table resource not found.\n");
		fseek(pFile, pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset, SEEK_SET);
		DWORD dwLengthOfResource = pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength;
		//��� Might want to store length of description???
		DWORD i;
		if (pPRFResourceDirectory->bIncludeDescriptions) {
			while (i = fgetc(pFile))
				--dwLengthOfResource;
			}		
		fSymbolTable->Read(pFile);
		}
	}

void PRF::Open(char *szFilename, char *szMode) {
	pFile = fopen(szFilename, szMode);
	if (pFile)
		GetFileInfo();
	}

DWORD PRF::GetResourceIndex(CHAR4 c4ResourceType, WORD wID)
{
	DWORD i = 0;
	while ((i < pPRFResourceDirectory->wNumOfResources) &&
			((pPRFResourceDirectory->prfResourceMap[i].c4Type != c4ResourceType) ||
			(pPRFResourceDirectory->prfResourceMap[i].wID != wID)))
		++i;
	return i;
}

void PRF::Add(char *szResourceFilename, CHAR4 c4ResourceType, WORD wID, char *szDescription) {
	DWORD dwResourceNumber;
	PRF_RESOURCEDIRECTORY *pNewPRFResourceDirectory;
	FILE *pFileToBeAdded;
	char buff[128];

	fflush(pFile);
	pFileToBeAdded = fopen(szResourceFilename, "rb");
	if( !pFileToBeAdded ) {
		sprintf("Error opening file: %s.\n", szResourceFilename);
		DebugString(buff);
		return;
		}

	Remove(c4ResourceType, wID);
	dwResourceNumber = pPRFResourceDirectory->wNumOfResources;

	pNewPRFResourceDirectory = (PRF_RESOURCEDIRECTORY*)new BYTE[pPRFFileHeader->dwSizeOfResourceDirectory + sizeof(PRF_RESOURCEMAP)];
	memcpy(pNewPRFResourceDirectory, pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory));
	delete[] pPRFResourceDirectory;
	pPRFResourceDirectory = pNewPRFResourceDirectory;
	pPRFFileHeader->dwSizeOfResourceDirectory += sizeof(PRF_RESOURCEMAP);
	++pPRFResourceDirectory->wNumOfResources;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].c4Type = c4ResourceType;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].wID = wID;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset =
			pPRFResourceDirectory->prfResourceMap[dwResourceNumber-1].dwOffset + 
			pPRFResourceDirectory->prfResourceMap[dwResourceNumber-1].dwLength;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength = 
			filelength(pFileToBeAdded) + 
			((pPRFResourceDirectory->bIncludeDescriptions) ? 
					(szDescription ? (strlen(szDescription)+1) : 1) : 0);
	pPRFFileHeader->dwResourceDirectoryOffset += pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength;

	// Add resource to file
	fseek(pFile, pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset, SEEK_SET);
	if (pPRFResourceDirectory->bIncludeDescriptions) {
		if (szDescription)
			fwrite(szDescription, strlen(szDescription)+1, 1, pFile);
		else
			putc(0, pFile);
		}
	fcopy(pFile, pFileToBeAdded, filelength(pFileToBeAdded));

	// Write resource directory at end of file.
	fwrite(pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory), 1, pFile);

	// Update file header.
	fseek(pFile, 0, SEEK_SET);
	fwrite(pPRFFileHeader, sizeof(PRF_FILEHEADER), 1, pFile);

	fclose(pFileToBeAdded);
	}

void PRF::Add1(void *pData, size_t dataSize, CHAR4 c4ResourceType, WORD wID, char *szDescription) {
	DWORD dwResourceNumber;
	PRF_RESOURCEDIRECTORY *pNewPRFResourceDirectory;

	if (pFile == NULL)
		return;

	fflush(pFile);

	Remove(c4ResourceType, wID);
	dwResourceNumber = pPRFResourceDirectory->wNumOfResources;

	pNewPRFResourceDirectory = (PRF_RESOURCEDIRECTORY*)new BYTE[pPRFFileHeader->dwSizeOfResourceDirectory + sizeof(PRF_RESOURCEMAP)];
	memcpy(pNewPRFResourceDirectory, pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory));
	delete[] pPRFResourceDirectory;
	pPRFResourceDirectory = pNewPRFResourceDirectory;
	pPRFFileHeader->dwSizeOfResourceDirectory += sizeof(PRF_RESOURCEMAP);
	++pPRFResourceDirectory->wNumOfResources;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].c4Type = c4ResourceType;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].wID = wID;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset =
			pPRFResourceDirectory->prfResourceMap[dwResourceNumber-1].dwOffset + 
			pPRFResourceDirectory->prfResourceMap[dwResourceNumber-1].dwLength;
	pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength = 
			dataSize + 
			((pPRFResourceDirectory->bIncludeDescriptions) ? 
					(szDescription ? (strlen(szDescription)+1) : 1) : 0);
	pPRFFileHeader->dwResourceDirectoryOffset += pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength;

	// Add resource to file
	fseek(pFile, pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset, SEEK_SET);
	if (pPRFResourceDirectory->bIncludeDescriptions) {
		if (szDescription)
			fwrite(szDescription, strlen(szDescription)+1, 1, pFile);
		else
			putc(0, pFile);
		}
	fwrite(pData, dataSize, 1, pFile);

	// Write resource directory at end of file.
	fwrite(pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory), 1, pFile);

	// Update file header.
	fseek(pFile, 0, SEEK_SET);
	fwrite(pPRFFileHeader, sizeof(PRF_FILEHEADER), 1, pFile);

	}

void PRF::CompactResourceMap(DWORD &dwResourceNumber, DWORD &dwDeltaOffset) {
	short i;
	for ( i = (short)dwResourceNumber; i < pPRFResourceDirectory->wNumOfResources; i++ ) {
		pPRFResourceDirectory->prfResourceMap[i] = pPRFResourceDirectory->prfResourceMap[i+1];
		pPRFResourceDirectory->prfResourceMap[i].dwOffset -= dwDeltaOffset;
		}
	}

void PRF::CompactResourceData(DWORD &dwResourceNumber) {
	size_t dataSize=0;
	short i;
	
	for ( i = (short)dwResourceNumber; i < pPRFResourceDirectory->wNumOfResources-1; i++ ) 
		dataSize += pPRFResourceDirectory->prfResourceMap[i+1].dwLength;
	
	fseek(pFile, pPRFResourceDirectory->prfResourceMap[dwResourceNumber+1].dwOffset, SEEK_SET);
	UniHandle *pHandle = new UniHandle(dataSize);
	void *pData = pHandle->Lock();
	fread((BYTE *)(pData), dataSize, 1, pFile);
	fseek(pFile, pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset, SEEK_SET);
	fwrite(pData, dataSize, 1, pFile);
	pHandle->Unlock();
	delete pHandle;
	}

void PRF::Remove(CHAR4 c4ResourceType, WORD wID) {
	DWORD dwResourceNumber;
	PRF_RESOURCEDIRECTORY *pNewPRFResourceDirectory;

	fflush(pFile);
	dwResourceNumber = GetResourceIndex(c4ResourceType, wID);

	// Update ResourceMap.
	if (dwResourceNumber <= (DWORD)pPRFResourceDirectory->wNumOfResources-1)	{	
		CompactResourceData(dwResourceNumber);
	
		DWORD dwDeltaOffset = pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength;
		CompactResourceMap(dwResourceNumber, dwDeltaOffset);

		pPRFFileHeader->dwSizeOfResourceDirectory -= sizeof(PRF_RESOURCEMAP);
		--pPRFResourceDirectory->wNumOfResources;
		pPRFFileHeader->dwResourceDirectoryOffset -= dwDeltaOffset;

		pNewPRFResourceDirectory = (PRF_RESOURCEDIRECTORY*)new BYTE[pPRFFileHeader->dwSizeOfResourceDirectory];
		memcpy(pNewPRFResourceDirectory, pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory));
		delete[] pPRFResourceDirectory;
		pPRFResourceDirectory = pNewPRFResourceDirectory;
		
		// Write resource directory at end of file.
		fwrite(pPRFResourceDirectory, (int)(pPRFFileHeader->dwSizeOfResourceDirectory), 1, pFile);

		// Update file header.
		fseek(pFile, 0, SEEK_SET);
		fwrite(pPRFFileHeader, sizeof(PRF_FILEHEADER), 1, pFile);
		}
	}

void PRF::Load(CHAR4 c4ResourceType, WORD wID, BYTE **data, int *numOfBytes)
	{
	DWORD dwResourceNumber;
	DWORD i;
	DWORD dwLengthOfResource;

	if (pFile == NULL)
		{
		return;
		}

	dwResourceNumber = GetResourceIndex(c4ResourceType, wID);
	if (dwResourceNumber < pPRFResourceDirectory->wNumOfResources)
		{ // Resource found
		//fflush(pFile);
		fseek(pFile, pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwOffset, SEEK_SET);
		dwLengthOfResource = pPRFResourceDirectory->prfResourceMap[dwResourceNumber].dwLength;
		if (pPRFResourceDirectory->bIncludeDescriptions)
			{
			while (i = fgetc(pFile))
				--dwLengthOfResource;
			}
		*numOfBytes = dwLengthOfResource;
		*data = new BYTE[dwLengthOfResource];
		fread(*data, dwLengthOfResource, 1, pFile);
		if(fSymbolTable)
			fSymbolTable->SwapResource((Ptr)data, c4ResourceType);
		}
	}

void PRF::SwapPrfFileHeader() {
	SwapBytes(&pPRFFileHeader->wByteOrder);
	SwapBytes(&pPRFFileHeader->dwVersion);
	SwapBytes(&pPRFFileHeader->dwFileSize);
	SwapBytes(&pPRFFileHeader->dwSizeOfResourceDirectory);
	SwapBytes(&pPRFFileHeader->dwResourceDirectoryOffset);
	}

void PRF::SwapPrfResourceDirectory() {
	SwapBytes(&pPRFResourceDirectory->wNumOfResources);	
	SwapBytes(&pPRFResourceDirectory->bIncludeDescriptions);	
	
	short i;
	
	for( i = 0; i<pPRFResourceDirectory->wNumOfResources; i++ )	{
		SwapBytes(&pPRFResourceDirectory->prfResourceMap[i].wID);
		SwapBytes(&pPRFResourceDirectory->prfResourceMap[i].dwOffset);
		SwapBytes(&pPRFResourceDirectory->prfResourceMap[i].dwLength);
		}
	}


