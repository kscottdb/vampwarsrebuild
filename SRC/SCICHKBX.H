#ifndef _scichkbx
#define _scichkbx

#include "scibuttn.h"

class sciCheckBox : public sciButton
{
	public:
		sciCheckBox(Boolean newSelected = FALSE);
		~sciCheckBox();
		Boolean HandleEvent(sciEvent &event);
		virtual void Select();
		virtual void Unselect();
		Boolean IsSelected();
		void UpdateCel();
		void SetCheckBoxCels(int newNormal, int newDepressed, int newDisabled,
			int newHilighted, int newNormalSelected, int newDepressedSelected,
			int newDisabledSelected, int newHilightedSelected);
		void SetFontCheckBox(int newNormalF, int newDepressedF, int newDisabledF,
			int newHilightedF, int newNormalSelectedF, int newDepressedSelectedF,
			int newDisabledSelectedF, int newHilightedSelectedF);
		void SetCheckBoxDefaultCels();
		void SetTextOffset(int newOffset);
		void Posn(int x, int y);

	protected:
		Boolean selected;

		int normalNonSelected;
		int depressedNonSelected;
		int disabledNonSelected;
		int hilightedNonSelected;
		int normalSelected;
		int depressedSelected;
		int disabledSelected;
		int hilightedSelected;

		int normalNonSelectedFont;
		int depressedNonSelectedFont;
		int disabledNonSelectedFont;
		int hilightedNonSelectedFont;
		int normalSelectedFont;
		int depressedSelectedFont;
		int disabledSelectedFont;
		int hilightedSelectedFont;


		int textOffset;
};



#endif // _scichkbx
