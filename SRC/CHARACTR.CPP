#include "os.h"
#include "app.h"
#include "resman.h"
#include "inifile.h"
#include "util.h"
#include "charactr.h"
#include "sciprop.h"
#include "sciscrip.h"
#include "scirail.h"
#include "scigame.h"
#include "scisound.h"
#include "sigs.h"
#include "random.h"
#include "rooms.h"
#include "game.h"

#include <time.h>
//#define CHATTER_DEBUG

extern SIGS *sigs;
extern Boolean CheckAttitude();
extern BOOL hdVersion;
extern BOOL lowMemory;

Random Character::randomGenerator;

#define LAST_HABIT_STATE 99
#define SLEEP_STATE 20
#define DONT_SLEEP

class CharacterHabitScript : public sciScript
	{
	public:
		Character *character;
		int lastAction;

		CharacterHabitScript(Character *newCharacter)
			: sciScript(NULL)
			{
			character = newCharacter;
			lastAction = game->GetTime();
			}

		void ChangeState(int newState)
			{
			sciRailCycle *railCycle;
			int i;

			if ((!sigs) && (!hdVersion) && CheckAttitude())
				{

				sciScript::ChangeState(newState);
				switch (state)
					{
					case 0:
						// Blink
						if (character->blinks)
							{
							//DebugString("Doing Blink for ");
							//DebugString(character->name);
							//DebugString(".\n");
							railCycle = new sciRailCycle(NULL);
							character->head->SetCycle(railCycle);
							railCycle->SetSound(character->sound);
							railCycle->AppendRandomSync(character->blinks);

							if (random(5) || lowMemory)
								{
								SetState(-1);
								}
							}
#ifndef DONT_SLEEP
						if (game->GetTime() > lastAction+5*60*60)
							SetNextState(SLEEP_STATE);
#endif
						SetSeconds(5 + random(5));
						break;
					case 1:
						// Nervous Habit
						if (character->numHabits)
							{
							//DebugString("Doing Nervous Habit for ");
							//DebugString(character->name);
							//DebugString(".\n");
							i = (2 * random(character->numHabits));
							if (character->facingEast)
								character->body->SetView(character->habits + i);
							else
								character->body->SetView(character->habits + i + 1);
							character->head->Hide();
							railCycle = new sciRailCycle(NULL);
							character->body->SetCycle(railCycle);
							railCycle->SetSound(character->sound);
							railCycle->AppendRandomSync(character->habits + i);
							SetSeconds(5 + random(5));
							}
						else
							{
							SetTicks(0);
							}
						break;
					case 2:
						character->head->Show();
						if (character->facingEast)
							((sciProp*)client)->SetView(character->bodyViewNumber);
						else
							((sciProp*)client)->SetView(character->bodyViewNumber + 1);
						SetState(-1);
						SetTicks(0);
						break;
					case SLEEP_STATE:
						if (character->numSleeps)
							{
							//DebugString("Doing Sleep1 for ");
							//DebugString(character->name);
							//DebugString(".\n");
							if (character->facingEast)
								character->body->SetView(character->sleeps);
							else
								character->body->SetView(character->sleeps + 1);
							character->head->Hide();
							railCycle = new sciRailCycle(this);
							character->body->SetCycle(railCycle);
							railCycle->SetSound(character->sound);
							railCycle->AppendRandomSync(character->sleeps);
							}
						else
							{
							SetTicks(0);
							SetNextState(2);
							}
						break;
					case SLEEP_STATE+1:
						//DebugString("Doing Sleep2 for ");
						//DebugString(character->name);
						//DebugString(".\n");
						if (character->facingEast)
							character->body->SetView(character->sleeps + 2);
						else
							character->body->SetView(character->sleeps + 3);
						character->head->Hide();
						railCycle = new sciRailCycle(this);
						character->body->SetCycle(railCycle);
						railCycle->SetSound(character->sound);
						railCycle->AppendRandomSync(character->sleeps + 2);
						SetNextState(SLEEP_STATE+1);
						break;
					case SLEEP_STATE+2:
						SetTicks(0);
						SetNextState(2);
						break;
					case LAST_HABIT_STATE:
						character->body->SetCycle(NULL);
						character->head->SetCycle(NULL);
						character->head->Show();
						if (character->facingEast)
							((sciProp*)client)->SetView(character->bodyViewNumber);
						else
							((sciProp*)client)->SetView(character->bodyViewNumber + 1);
						break;
					}
				}
			}
	};

class CharacterTalkingSubScript : public sciScript
	{
	public:
		~CharacterTalkingSubScript()
			{
			// Who's doing this.
			}
		void ChangeState(int newState)
			{
			sciScript::ChangeState(newState);
			switch (state)
				{
				case 0:
					// Wait for the cue.
					break;
				case 1:
					((sciScript*)client)->SetScript(NULL);
					//delete this;
					break;
				}
			}
	};

class CharacterTalkingMainScript : public sciScript
	{
	public:
		Character *character;

		CharacterTalkingMainScript(sciScript *newCaller, Character *newCharacter)
			: sciScript(newCaller)
			{
			character = newCharacter;
			}
		void ChangeState(int newState)
			{
			sciScript::ChangeState(newState);
			switch (state)
				{
				case 0:
					// Wait for the cue.
					break;
				case 1:
					((sciProp*)client)->SetScript(new
						CharacterHabitScript(character));
					break;
				}
			}
	};

Character::Character(int newPosition, int newCharacterNumber)
	{
	name = NULL;
	resourceManager = NULL;
	sound = NULL;
	head = new sciProp;
	body = new sciProp;
	body->SetPri(111);
	head->SetPri(110);
	position = newPosition;
	characterNumber = newCharacterNumber;

	if (sigs)
		{
		randomGenerator.Seed(0);
		}
	else
		{
		randomGenerator.Seed(time(NULL));
		}
	}

Character::Character()
	{
	name = 0;
	head = 0;
	body = 0;
	resourceManager = pApp->pResourceManager;
	sound = NULL;
	numHabits = 0;
	blinks = 0;
	numSleeps = 0;
	bodyViewNumber = 0;
	if (sigs)
		{
		randomGenerator.Seed(0);
		}
	else
		{
		randomGenerator.Seed(time(NULL));
		}
	}

Character::~Character()
	{
	StopTalking();
	if (head)
		delete head;
	if (body)
		delete body;
	if (name)
		delete[] name;
	if (resourceManager && (pApp->pResourceManager != resourceManager))
		delete resourceManager;
	if (sound)
		delete sound;
	}

void Character::SetName(char *newName)
	{
	if (name)
		delete[] name;
	name = new char[strlen(newName) + 1];
	strcpy_s(name, strlen(newName) + 1, newName);
	}

void Character::ShowCharacter()
	{
	SetPosition(position);
	if (headViewNumber)
		{
		head->Show();
		}
	body->Show();
	body->SetScript(new CharacterHabitScript(this));
	}

void Character::HideCharacter()
	{
	head->Hide();
	head->SetScript(NULL);
	body->Hide();
	body->SetScript(NULL);
	}

void Character::DoBoth(sciScript *caller, int theBody, int theSync)
	{
	sciRailCycle *rail;
	sciScript *script;

	if (syncNumber == 0)
		{
		if (caller)
			caller->Cue();
		return;
		}
#ifdef CHATTER_DEBUG
	char chChatterDebug[50];
	sprintf(chChatterDebug,"Character Both Sync %s %d\n", name, syncNumber + theSync*syncOffset);
	DebugString(chChatterDebug);
	sprintf(chChatterDebug,"Character Both theBody=%d theSync=%d\n", theBody, theSync);
	DebugString(chChatterDebug);
#endif

	if (body->script)
		{
		body->script->SetState(LAST_HABIT_STATE-1);
		body->script->Cue();
		}

	script = new CharacterTalkingMainScript(caller, this);
	body->SetScript(script);
	body->script->Doit();
	if ((theBody != -1) && (theSync != -1))
		{
		script->SetScript(new CharacterTalkingSubScript);
		script->script->Doit();
		}
	if (theSync != -1)
		{
		if (theBody != -1)
			{
			rail = new sciRailCycle(script->script);
			}
		else
			{
			rail = new sciRailCycle(script);
			}
		head->SetCycle(rail);
		rail->SetSound(sound);
		rail->AppendRandomSync(syncNumber + theSync*syncOffset);
		}

	if (theBody != -1)
		{
		if (facingEast)
			body->SetView(bodyViewNumber + theBody);
		else
			body->SetView(bodyViewNumber + theBody + 1);
		rail = new sciRailCycle(script);
		body->SetCycle(rail);
		rail->SetSound(sound);
		rail->AppendRandomSync(bodyViewNumber + theBody);
		}
	}

void Character::DoHands(sciScript *caller, int theBody)
	{
	DoBoth(caller, theBody, -1);
	}

void Character::DoTalking(sciScript *caller, int theSync)
	{
	DoBoth(caller, -1, theSync);
	}

Boolean CheckAttitude()
	{
	return TRUE;
	}

void Character::DoIWon(sciScript *caller)
	{
	}

void Character::DoILost(sciScript *caller)
	{
	}

void Character::SetPosition(int newPosition)
	{
	}

void Character::StopTalking()
	{
	if (head)
		if ((sciRailCycle*)(head->cycler))
			{
			//DebugString("Stopping head.\n");
			((sciRailCycle*)(head->cycler))->StopSyncing();
			}
	if (body)
		if ((sciRailCycle*)(body->cycler))
			{
			//DebugString("Stopping body.\n");
			((sciRailCycle*)(body->cycler))->StopSyncing();
			}
	}
