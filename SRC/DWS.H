/******************************************************************************
File:      dws.h
Version:   2.00
Tab stops: every 2 columns
Project:   DiamondWare's Sound ToolKit for Windows
Copyright: 1994-1996 DiamondWare, Ltd.  All rights reserved.
Written:   96/07/11 by John Lundy
Purpose:   Contains declarations for the DW Sound ToolKit for Windows
History:   96/07/11 JCL Started
           96/09/17/JCL
******************************************************************************/



#ifndef dws_INCLUDE

  #define dws_INCLUDE



	/*lint ++flb*/



  #if defined (__WATCOMC__) && !defined (__WINDOWS__)
    typedef unsigned char  BYTE;
    typedef unsigned short WORD;
		typedef unsigned long  DWORD;
    typedef unsigned int   UINT;
  #endif

  #if defined(__NT__) && !defined(WIN32)
    #define WIN32
  #endif

  #if defined(_WIN32) && !defined(WIN32)
    #define WIN32
  #endif

  #if defined(WIN32) && !defined(__BORLANDC__)
    #define dws_ENTRY __declspec(dllexport) CALLBACK
  #else
    #define dws_ENTRY __export CALLBACK
  #endif

  #ifndef WIN32
    #if defined(__WATCOMC__) && defined(__WINDOWS_386__)
      #define dws_DIST
    #else
      #define dws_DIST _far
    #endif
  #else
    #define dws_DIST
  #endif

  /* four bytes in length and unsigned */
	#ifndef	DWORD
    #define DWORD unsigned long
    #define dws_DWORDDEFINED
  #endif

  /* two bytes in length and unsigned */
	#ifndef	WORD
    #define WORD  unsigned short
    #define dws_WORDDEFINED
  #endif

  /* one byte in length and unsigned */
	#ifndef	BYTE
    #define BYTE  unsigned char
    #define dws_BYTEDEFINED
  #endif

/*****************************************************************************/
  /*
   . If a call to a dws_ function returns 0 (FALSE), then this is an
   . indication that an error has occured.  Call dws_ErrNo to see which
   . one.  The following series of #defines is the complete list of
   . possible return values for dws_ErrNo.
  */
  #define dws_EZERO                        0  /* no error */

	/* These errors may be triggered by any dws_ function */
  #define dws_NOTINITTED                   1
  #define dws_ALREADYINITTED               2
  #define dws_NOTSUPPORTED                 3
  #define dws_INTERNALERROR                4
  #define dws_INVALIDPOINTER               5
  #define dws_RESOURCEINUSE                6
  #define dws_MEMORYALLOCFAILED            7
	#define dws_SETEVENTFAILED							 8
	#define dws_NOWINDOW										 9
	#define dws_TIMERFAILURE								10

  /*
   . dws_BUSY may be triggered during a call to a dws_ function, if any
   . dws_ function is already executing.  Ordinarily, this should never
   . occur.
  */
	#define dws_BUSY												11

	/* These errors may be triggered only by dws_Init */
  #define dws_Init_BUFTOOSMALL           101
  #define dws_Init_NOSUCHDEVICE          102
  #define dws_Init_NOSUCHCAPS            103

	/* These errors may be triggered by any dws_D (dig) function */
  #define dws_D_NOTADWD                  201
  #define dws_D_NOTSUPPORTEDVER          202
  #define dws_D_BADDPLAY                 203
	#define dws_D_BADDREC 								 204
	#define dws_D_NOTRECORDING						 205
	#define dws_D_ALREADYRECORDING				 206
	#define dws_D_BAD3DPLAY 							 207

	/* This error may be triggered only by dws_DPlay */
  #define dws_DPlay_NOSPACEFORSOUND      251

	/* These errors may be triggered only by dws_WAV2DWD */
  #define dws_WAV2DWD_NOTAWAVE           301
	#define dws_WAV2DWD_UNSUPPORTEDFORMAT  302
	#define dws_WAV2DWD_CANTCONVERT 			 303

	/* This error may be triggered by any dws_M (music) function */
	#define dws_M_BADMPLAY								 401
	#define dws_M_BADSONGNUM							 402

	#define dws_MPlay_NOSPACEFORSONG			 451

	#define dws_MID2DWM_NOTAMID 					 501
	#define dws_MID2DWM_INVALID 					 502

	#define dws_CD_TRACKNOTAUDIO					 601
	#define dws_CD_BADOFFSET							 602
	#define dws_CD_BADCDPLAY							 603

	/* These errors may be triggered by any dws_DSP function */
	#define dws_DSP_CANTLOADMOD 					 701
	#define dws_DSP_INVALIDMOD						 702
	#define dws_DSP_BADCOMMAND						 703


/*---------------------------------------------------------------------------*/
  /*
   . This section #defines bitfields which are used by various dws_
   . functions.  Each bit in a bitfield, by definition, may be set/reset
   . independently of all other bits.
  */

  /* flags for muscaps value of dws_DETECTRESULTS structure */
  #define dws_muscap_NONE      0x0000 /* no music playback */
  #define dws_muscap_MIDIPORT  0x0001 /* output port */
  #define dws_muscap_SYNTH     0x0002 /* generic int synth */
  #define dws_muscap_SQSYNTH   0x0004 /* square wave int synth */
  #define dws_muscap_FMSYNTH   0x0008 /* FM int synth */
  #define dws_muscap_MAPPER    0x0010 /* MIDI mapper */
	#define dws_muscap_ANY			 0x001f /* mask of all available modes */

	#define dws_muscap_DLS			 0x0020 /* Supports downloadable samples */


  #define dws_digcap_NONE        0x0000 /* no digitized sound playback */
  #define dws_digcap_11025_08_1  0x0001 /* 11kHz, 8-bit, mono */
  #define dws_digcap_11025_08_2  0x0002 /* 11kHz, 8-bit, stereo */
  #define dws_digcap_11025_16_1  0x0004 /* 11kHz, 16-bit, mono */
  #define dws_digcap_11025_16_2  0x0008 /* 11kHz, 16-bit, stereo */
  #define dws_digcap_22050_08_1  0x0010 /* 22kHz, 8-bit, mono */
  #define dws_digcap_22050_08_2  0x0020 /* 22kHz, 8-bit, stereo */
  #define dws_digcap_22050_16_1  0x0040 /* 22kHz, 16-bit, mono */
  #define dws_digcap_22050_16_2  0x0080 /* 22kHz, 16-bit, stereo */
  #define dws_digcap_44100_08_1  0x0100 /* 44kHz, 8-bit, mono */
  #define dws_digcap_44100_08_2  0x0200 /* 44kHz, 8-bit, stereo */
  #define dws_digcap_44100_16_1  0x0400 /* 44kHz, 16-bit, mono */
  #define dws_digcap_44100_16_2  0x0800 /* 44kHz, 16-bit, stereo */
	#define dws_digcap_ALLRATES 	 0x0fff
	#define dws_digcap_TWEENRATES  0x1000 /* Supports all in-between rates */
	#define dws_digcap_EXTMIXER 	 0x2000 /* In Win32, this means DirectSound */
	#define dws_digcap_HWMIXER		 0x4000 /* (DirSnd only) hardware mixer */
	#define dws_digcap_HW3D 			 0x8000 /* (DirSnd only) hardware 3D proc. */
	#define dws_digcap_ANY				 0x1fff /* mask of all available modes */
	#define dws_digcap_NUM				 12 		/* 12 unique modes */

  /*
   . The following #defines are bitfields used in the flags field in the
   . dws_IDEAL struct.
   .
   . SWAPLR should be used to compensate for downstream electronics which
   . switch the left and right field.
   .
   . Since pitch and volume change both require some additional CPU time,
   . you may want to disable them for slow machines and enable for fast
   . machines.  This is an easy way for you to do this, without changing
   . your program around.
   .
   . Notes:
   .   1) Pitch change is slower than volume change.
   .   2) Pitch decrease (dws_DPLAY.pitch > dws_IDENTITY) is slower than
   .      pitch increase
   .   3) When the source sound, or the STK, is 8-bit, raising the volume
   .      is slower than lowering it.  Otherwise both are equal.
  */
	#define dws_ideal_NONE						0x0000	/* this is the normal case */
	#define dws_ideal_SWAPLR					0x0001	/* swap Left & Right on playback */
	#define dws_ideal_DISABLEPITCH		0x0002	/* disable the pitch feature */
	#define dws_ideal_DISABLEVOLUME 	0x0004	/* disable the volume feature */
	#define dws_ideal_MAXSPEED				0x0006	/* use this for very slow machines */
	#define dws_ideal_DISABLEDSP			0x0008	/* disable the DSP features */
	#define dws_ideal_DISABLETHREADS	0x0010	/* disable the use of threads */
	#define dws_ideal_PITCHCOMPENSATE 0x0020	/* play DWD at correct pitch */
	#define dws_ideal_INVERTPHASE 		0x0040	/* correct for mis-wired equip. */

  /*
   . These are the flag bitfields for the dws_DPLAY structure.
   . Each corresponds to one field in the struct.
  */
	#define dws_dplay_SND 					0x80000000L
	#define dws_dplay_COUNT 				0x40000000L
	#define dws_dplay_PRIORITY			0x20000000L
	#define dws_dplay_PRESND				0x10000000L
	#define dws_dplay_SOUNDNUM			0x08000000L
	#define dws_dplay_LVOL					0x04000000L
	#define dws_dplay_RVOL					0x02000000L
	#define dws_dplay_PITCH 				0x01000000L
	#define dws_dplay_CALLBACK			0x00800000L
	#define dws_dplay_FUNCPTR 			0x00400000L
	#define dws_dplay_SYNCHRONOUS 	0x00200000L
	#define dws_dplay_FIRSTSAMPLE 	0x00100000L
	#define dws_dplay_CURSAMPLE 		0x00080000L
	#define dws_dplay_LASTSAMPLE		0x00040000L
	#define dws_dplay_PAUSE 				0x00020000L
	#define dws_dplay_AUTORATECONV	0x00010000L
	#define dws_dplay_POSITION			0x00008000L
	#define dws_dplay_3DPLAY				0x00004000L
	#define dws_dplay_DSPPLAY 			0x00002000L
	#define dws_dplay_NEXT					0x00001000L

	#define dws_dplay_ALL 					0xffffc000L


	#define dws_3dplay_POSITION 	0x80000000L
	#define dws_3dplay_VELOCITY 	0x40000000L
	#define dws_3dplay_ORIENT 		0x20000000L
	#define dws_3dplay_MINDIST		0x10000000L
	#define dws_3dplay_MAXDIST		0x08000000L
	#define dws_3dplay_ANGLE			0x04000000L
	#define dws_3dplay_OUTVOLUME	0x02000000L


	#define dws_3dworld_POSITION	0x80000000L
	#define dws_3dworld_VELOCITY	0x40000000L
	#define dws_3dworld_ORIENT		0x20000000L
	#define dws_3dworld_METERS		0x10000000L
	#define dws_3dworld_ROLLOFF 	0x08000000L
	#define dws_3dworld_DOPPLER 	0x04000000L


  /*
	 . These are the flag bitfields for the dws_DRECORD structure.
   . Each corresponds to one field in the struct.
  */
	#define dws_drec_DEV					0x80000000L
	#define dws_drec_TYP					0x40000000L
	#define dws_drec_BUF					0x20000000L
	#define dws_drec_COUNT				0x10000000L
	#define dws_drec_BUFLEN 			0x08000000L
	#define dws_drec_NUMBUFS			0x04000000L
	#define dws_drec_CALLBACK 		0x02000000L
	#define dws_drec_FUNCPTR			0x01000000L
	#define dws_drec_COMPRESS 		0x00800000L


	#define dws_mplay_SONG				0x80000000L
	#define dws_mplay_COUNT 			0x40000000L
	#define dws_mplay_PRIORITY		0x20000000L
	#define dws_mplay_SONGNUM 		0x10000000L
	#define dws_mplay_TRKNUM			0x08000000L
	#define dws_mplay_VOL 				0x04000000L
	#define dws_mplay_TRKVOL			0x02000000L
	#define dws_mplay_PITCH 			0x01000000L
	#define dws_mplay_TRKPITCH		0x00800000L
	#define dws_mplay_CALLBACK		0x00400000L
	#define dws_mplay_FUNCPTR 		0x00200000L
	#define dws_mplay_SYNCHRONOUS 0x00100000L
	#define dws_mplay_FIRSTTIME 	0x00080000L
	#define dws_mplay_CURTIME 		0x00040000L
	#define dws_mplay_LASTTIME		0x00020000L
	#define dws_mplay_PAUSE 			0x00010000L
	#define dws_mplay_NEXT				0x00008000L


	#define dws_cdplay_STTRACK		0x80000000L
	#define dws_cdplay_STOFF			0x40000000L
	#define dws_cdplay_ENDTRACK 	0x20000000L
	#define dws_cdplay_ENDOFF 		0x10000000L
	#define dws_cdplay_COUNT			0x08000000L
	#define dws_cdplay_CALLBACK 	0x04000000L
	#define dws_cdplay_FUNCPTR		0x02000000L


  /* The following 2 consts indicate the status of music playback */
  #define dws_MSONGSTATUSPLAYING  0x0001
  #define dws_MSONGSTATUSPAUSED   0x0002

  
  /*
   . These are the flag bitfields for the dws_DSP structure
   . Each corresponds to a field in the struct.
  */
  #define dws_dsp_DSPMOD  0x00000001L
  #define dws_dsp_DSPFUNC 0x00000002L
  #define dws_dsp_PARAM   0x00000004L

  
  /*
   . These are the flag bitfields for the dws_DSPPLAY structure
   . Each corresponds to an element in the array.
  */
  #define dws_dspplay_DSP1     0x00000001L
  #define dws_dspplay_DSP2     0x00000002L
  #define dws_dspplay_DSP3     0x00000004L
  #define dws_dspplay_DSP4     0x00000008L
  #define dws_dspplay_DSP5     0x00000010L
  #define dws_dspplay_DSP6     0x00000020L
  #define dws_dspplay_DSP7     0x00000040L
  #define dws_dspplay_DSP8     0x00000080L


/*---------------------------------------------------------------------------*/
  /*
   . When the WIN-STK sends a message to your window, the lParam field
   . indicates the purpose of the message.
  */
  #define dws_event_SOUNDCOMPLETE  0x0001
  #define dws_event_SOUNDSTARTED   0x0002
  #define dws_event_SOUNDABORTED   0x0003
	#define dws_event_SOUNDLOOPED 	 0x0004

	#define dws_event_RECORDCOMPLETE 0x0005
	#define dws_event_RECORDABORTED  0x0006

	#define dws_event_CDCOMPLETE		 0x0007
	#define dws_event_CDLOOPED			 0x0008
	#define dws_event_CDABORTED 		 0x0009

	#define dws_event_SONGCOMPLETE	0x0010
	#define dws_event_SONGSTARTED 	0x0011
	#define dws_event_SONGABORTED 	0x0012
	#define dws_event_SONGLOOPED		0x0013


	#define dws_cdstatus_NOTREADY 				0x0001
	#define dws_cdstatus_OPEN 						0x0002
	#define dws_cdstatus_DISCHASNOTRACKS	0x0004
	#define dws_cdstatus_DISCNOTAUDIO 		0x0005
	#define dws_cdstatus_PLAY 						0x0006
	#define dws_cdstatus_PAUSE						0x0007
	#define dws_cdstatus_STOP 						0x0008

  /* For no change in volume level or pitch */
  #define dws_IDENTITY 0x100

  /* Default priority, if not specified in call to dws_DPlay */
  #define dws_NORMALPRIORITY 0x1000


	#define dws_NAMELEN 128


	/* The total number of drivers for each supported operation */
	#define dws_NUMDRIVERS 8

	/* The total number of DSP effects allowed on a sound */
	#define dws_NUMDSPS 	 8


	#define dws_NUMRECORDBUFS 32		//max number of buffers (dws_DRECORD struct)


	#define dws_NUMCOMPRESSIONS 16



/*****************************************************************************/
  /* struct member byte alignment - begin */
  #ifdef _MSC_VER
    #ifdef WIN32
      #pragma pack(push, dws_h, 2) /* Use 2 byte alignment for these structs */
    #else
      #pragma pack(2)
    #endif
  #endif

  #if defined(__SC__) || defined(__WATCOMC__)
    #pragma pack(push, 2) /* Use 2 byte alignment for these structs */
  #endif


	/* Music output callback */
	typedef void dws_MPLAYCB(DWORD msg, DWORD songnum, DWORD event);

	/* CD callback */
	typedef void dws_CDPLAYCB(DWORD msg, DWORD event);

	/* Digitized recording callback */
	typedef void dws_DRECORDCB(DWORD msg, DWORD bufnum, DWORD event);

	/* Digitized playback callback */
	typedef void dws_DPLAYCB(DWORD msg, DWORD soundnum, DWORD event);


  /*
   . This section declares the struct types used by the STK.  In each
   . case, the user must create an instance of the struct prior to making
   . a call to any STK function which takes a pointer to it.  The STK does
   . not keep a pointer to any of these structs internally; after the call
   . returns, you may deallocate it, if you wish.
   .
   . NB: The STK _does_ keep pointers to digitized sound buffers!
  */

	typedef struct
	{
		char name[dws_NAMELEN];
		DWORD muscaps;
		DWORD numvoices;

	} dws_MUSDESC;

	typedef struct
	{
		char name[dws_NAMELEN]; 			//this is a printable string
		char name2nd[dws_NAMELEN];		//this is also a printable string
		DWORD digcaps;								//see dws_digcap_xxxxxx #defines above
		DWORD digbfr; 								//not for use by the app
		DWORD digdevn[4]; 						//not for use by the app

	} dws_DIGDESC;

	typedef struct
	{
		DWORD digincaps;							//see dws_digincap_xxxxxx #defines above
		DWORD diginbfr; 							//not for use by the app
		DWORD digindevnum;						//not for use by the app
		/*
		 . The following crazy-looking array needs some explanation.	For every
		 . digitized input driver, for every mode it supports, for every digitized
		 . output driver...there is a bitfield of what is full duplex (simultaneous
		 . record and playback).
		*/
		DWORD diginfdx[dws_digcap_NUM][dws_NUMDRIVERS];

	} dws_DIGINDESC;

	typedef struct
	{
		char name[dws_NAMELEN];

		/* Not for application use */
		DWORD masterid;
		DWORD masterchans;
		DWORD digoid;
		DWORD digochans;
		DWORD musoid;
		DWORD musochans;
		DWORD cdid;
		DWORD cdchans;

	} dws_MIXDESC;

  /*
   . A pointer to this struct is passed to dws_DetectHardWare, which fills
   . it in.  It should then be passed to dws_Init.  If you plan on writing
   . this struct out to a file, it's important that you write the entire
   . contents.  There is information (for internal STK use only) in the
   . reserved[] field!
  */
  typedef struct
  {
		DWORD nummus; 										//number of music devices
		dws_MUSDESC musdesc[dws_NUMDRIVERS];

		DWORD nummusin; 									//number of music input devices
		DWORD musincaps[dws_NUMDRIVERS];	//see dws_muscap_xxxxxx #defines above

		DWORD numdigs;										//number of digitized audio devices
		dws_DIGDESC digdesc[dws_NUMDRIVERS];

		DWORD numdigin; 									//number of digitized input devices
		dws_DIGINDESC digindesc[dws_NUMDRIVERS];

		DWORD numcds;

		DWORD nummixers;
		dws_MIXDESC mixdesc[dws_NUMDRIVERS];

		BYTE reserved[48];								//undocumented!

  } dws_DETECTRESULTS;


  /*
   . A pointer to this struct is passed as a parameter to dws_Init.  This
   . struct allows the program to let the STK know what capabilities it
   . will actually use.
  */
  typedef struct
  {
    DWORD flags;         /* use combination of dws_ideal_xxxx               */

		DWORD musdev; 			 /* number of music output device to use						*/
		DWORD musnsongs;		 /* maximum number of music songs (files) you'll use*/

    DWORD musindev;      /* number of music input device to use             */

    DWORD digdev;        /* number of dig output device to use              */
    DWORD digtyp;        /* use dws_digcap_xxxxxx (mode) to select          */
		DWORD dignvoices; 	 /* maximum number of digitized voices you'll use   */
		DWORD digseqlen;

		DWORD cddev;

		DWORD mixdev;

		HWND hwnd;					 /* Optionally can be set to NULL--see the docs */

    BYTE  reserved[14];  /* make sure it occupies full paragraphs plus some */

  } dws_IDEAL;


  /*
   . 3D spatialized sound is produced with the use of these parameters.
  */
  typedef struct
  {
		DWORD flags;				 /* flag indicates active fields in struct */
		float xpos; 				 //listener
		float ypos; 				 //listener
		float zpos; 				 //listener
		float xvel; 				 //listener
		float yvel; 				 //listener
		float zvel; 				 //listener
		float xorientfront;
		float yorientfront;
		float zorientfront;
		float xorienttop;
		float yorienttop;
		float zorienttop;
		float metersperdist; //3D world
		float rolloffmult;	 //3D world
		float dopplermult;	 //3D world

	} dws_3DWORLD;

  typedef struct
  {
		DWORD flags;				 /* flag indicates active fields in struct */
		float xpos;
		float ypos;
		float zpos;
		float xvel;
		float yvel;
		float zvel;
		float xorient;
		float yorient;
		float zorient;
		float mindist;
		float maxdist;
		DWORD inangle;
		DWORD outangle;
		DWORD outvolume;

  } dws_3DPLAY;


  /*
   . Each DSP can have a variable number of parameters for the dsp
   . computations. The elements dspmod and dspfunc are required.
   . The parameters are specific to the DSP being used. See the
   . documentation for the DSP module to determine which, if any
   . parameters need to be passed.
  */
  typedef struct
  {
  	DWORD flags;         /* flag indicates active fields in struct      */
  	DWORD dspmod;        /* ordinal of dsp module to use                */
  	DWORD dspfunc;       /* ordinal of dsp function within module       */
		DWORD param;				 /* can be param or ptr to param struct for dsp */

  } dws_DSP;


  /*
   . Each sound can have up to eight DSP routines operate on it's
   . data before the sound is played.
   .
   . There is one flag bit for each element of the dsp[] array.
  */
  typedef struct
  {
		DWORD flags;							/* flag indicates active fields in struct */
		dws_DSP dsp[dws_NUMDSPS]; /* can have 8 different dsps per sound		*/

  } dws_DSPPLAY;


  /*
   . A pointer to this struct is passed to dws_DPlay.
   .
   . NB: The soundnum field is filled in by dws_DPlay as a return value.
  */
	typedef struct dws_DPLAY_T
  {
    DWORD flags;         /* flag indicates active fields in struct         */
    BYTE  dws_DIST *snd; /* pointer to buffer which holds a .DWD file      */
    WORD  count;         /* number of times to play, or 0=infinite loop    */
    WORD  priority;      /* higher numbers mean higher priority            */
    WORD  presnd;        /* soundnum to sequence sound _after_             */
    WORD  soundnum;      /* dws_DPlay returns a snd number from 10-65535   */
    WORD  lvol;          /* 0-65535, 0 is off, 256 is dws_IDENTITY         */
    WORD  rvol;          /* if the output is mono lvol & rvol are averaged */
    WORD  pitch;         /* 1-65535, 256 is dws_IDENTITY (0 is unuseable)  */
    WORD  dummy;         /* added to insure DWORD alignment                */
    HWND  hwndmsg;       /* handle of window to which to send msg          */
		UINT	message;			 /* message sent to window hwndmsg								 */
		void *funcptr;
		DWORD synchronous;
    DWORD firstsample;   /* first sample of sound buffer for play or loop  */
    DWORD cursample;     /* current sample playing within sound            */
    DWORD lastsample;    /* last sample of sound buffer for play or loop   */
		DWORD pause;
		DWORD autorateconv;  //for DS only?
		DWORD position;
    dws_3DPLAY  dddplay; /* 3D spatialized sound parameters                */
		dws_DSPPLAY dspplay; /* dsp struct with dsps and parameters for sound  */
		struct dws_DPLAY_T *next;

    #ifndef WIN32
      BYTE reserved[4];  /* make sure it occupies full paragraphs          */
    #endif

  } dws_DPLAY;


	/* A pointer to this struct is passed to dws_DRecord. */
  typedef struct
	{
		DWORD flags;						//count, callback, synchronous
		DWORD dev;							//number of dig input device to use
		DWORD typ;							//use dws_digincap_xxxxxx (mode) to select
		BYTE **buf;
		DWORD count;
		DWORD buflen;
		DWORD numbufs;
		HWND	hwndmsg;
		UINT	message;
		void *funcptr;
		DWORD compresstyp;
		DWORD duplex; 					//(return value) 1=half duplex, 2=full
		BYTE reserved[12];

	} dws_DRECORD;


  /* A pointer to this struct is passed to dws_MPlay. */
	typedef struct dws_MPLAY_T
	{
		DWORD flags;
		BYTE dws_DIST *song;		/* pointer to buffer which holds a .DWM file		*/
		WORD count; 						/* number of times to play, or 0=infinite loop	*/
		WORD priority;
		WORD songnum;
		DWORD trknum;
		WORD vol;
		WORD trkvol;
		WORD pitch;
		HWND hwndmsg;
		UINT message;
		void *funcptr;
		DWORD synchronous;
		DWORD firsttime;
		DWORD curtime;
		DWORD lasttime;
		DWORD pause;
		struct dws_MPLAY_T *next;

    BYTE reserved[10];      /* make sure it occupies full paragraphs        */

  } dws_MPLAY;


	typedef struct
	{
		DWORD status; 					//see dws_cdstatus_* above
		DWORD curtrack; 				//(only if playing or paused) 1-N is track num
		DWORD curpos; 					//(only if playing or paused) pos is in msec
		DWORD numtracks;
		DWORD lmediaid;
		DWORD hmediaid;
		DWORD lupccode;
		DWORD hupccode;

	} dws_CDINFO;

	/*
	 . The sttrack field specifies the starting track; otherwise it's track 1
	 . stoff is optional, which specifies the offset into the track.
	 . If no endtrack or endoff is specified, the CD will play until EOD[isc].
	 . If endtrack is specified, but not endoff, then CD will play until EOT.
	 . This can be used to play only one track, set endtrack == sttrack, and
	 . omit endoff.
	*/
	typedef struct
	{
		DWORD flags;
		DWORD sttrack;					// tracks are numbered 1 to N
		DWORD stoff;						// in milliseconds from start of track
		DWORD endtrack;
		DWORD endoff;
		DWORD count;						// 1-N, or 0 for infinite loop
		HWND	hwndmsg;					// handle of window to which to send msg
		UINT	message;					// message sent to window hwndmsg
		void *funcptr;

	} dws_CDPLAY;



  /* struct member byte alignment - end */
  #ifdef _MSC_VER
    #ifdef  WIN32
      #pragma pack(pop, dws_h) /* back to default packing */
    #else
      #pragma pack()
    #endif
  #endif

  #if defined(__SC__) || defined(__WATCOMC__)
    #pragma pack(pop) /* back to default packing */
  #endif

/*****************************************************************************/
  /* This section prototypes the WIN-STK functions */

  #ifdef __cplusplus
    extern "C" {
  #endif



	/* These function are callable at any time. */

	/* Returns the major/minor WIN-STK DLL version. */
	WORD dws_ENTRY dws_GetVersion(void);

	/* Returns the number of the last error which occured. */
	WORD dws_ENTRY dws_ErrNo(void); 		//major and minor, little-endian

/*---------------------------------------------------------------------------*/


  /*
   . Each function in this section has a boolean return value.  A 0 (FALSE)
   . indicates that the function failed in some way.  In this case, call
   . dws_ErrNo to get the specific error.  Otherwise, a return value of 1
   . (TRUE) indicates that all is well.
  */
  WORD dws_ENTRY dws_DetectHardWare(dws_DETECTRESULTS dws_DIST *dr);

  WORD dws_ENTRY dws_Init(dws_DETECTRESULTS dws_DIST *dr,
                                   dws_IDEAL dws_DIST *ideal);


  /*
   . If the program has called dws_Init, it _MUST_ call dws_Kill before it
   . terminates.
  */
  WORD dws_ENTRY dws_Kill(void);


  /*
	 . The following functions control the software mixers inside the STK.	A
   . value of 0 is off; dws_IDENTITY is normal, and 0xffff is maximum
   . volume (grossly distorted).
  */
	WORD dws_ENTRY dws_XMaster(WORD lvolume, WORD rvolume);

	WORD dws_ENTRY dws_XDig(WORD lvolume, WORD rvolume);

	WORD dws_ENTRY dws_XMusic(WORD lvolume, WORD rvolume);


	/*
	 . These functions, on the other hand, control the actual settings of
	 . the sound hardware's volume.  Not only that, but these settings are
	 . persistent!	You may wish to save the old values before you write over
	 . then, and restore them when your application shuts down.
	*/
	WORD dws_ENTRY dws_VSetMaster(WORD lvolume, WORD rvolume);

	WORD dws_ENTRY dws_VSetDig(WORD lvolume, WORD rvolume);

	WORD dws_ENTRY dws_VSetMusic(WORD lvolume, WORD rvolume);

	WORD dws_ENTRY dws_VSetCD(WORD lvolume, WORD rvolume);

	WORD dws_ENTRY dws_VGetMaster(WORD *lvolume, WORD *rvolume);

	WORD dws_ENTRY dws_VGetDig(WORD *lvolume, WORD *rvolume);

	WORD dws_ENTRY dws_VGetMusic(WORD *lvolume, WORD *rvolume);

	WORD dws_ENTRY dws_VGetCD(WORD *lvolume, WORD *rvolume);


  /*
   . The following 9 functions comprise the digitized sound functions of
   . the STK.  See the documentation for complete details.
  */
  WORD dws_ENTRY dws_DPlay(dws_DPLAY dws_DIST *dplay);

  /*
   . Allows you to change the following items:
   .   count
   .   priority
   .   lvol
   .   rvol
   .   pitch
   .   firstsample
   .   cursample
   .   lastsample
   .   hwndmsg and message (simultaneously)
  */
	WORD dws_ENTRY dws_DSetInfo(dws_DPLAY dws_DIST *dplay1);

  /*
   . Takes a ptr to a dws_DPLAY struct; soundnum specifies which sound.
   .
   . Can retrieve the current values for any or all of the following:
   .   snd
   .   count
   .   priority
   .   presnd
   .   lvol
   .   rvol
   .   pitch
   .   firstsample
   .   cursample
   .   lastsample
   .   hwndmsg and message (simultaneously)
  */
	WORD dws_ENTRY dws_DGetInfo(dws_DPLAY dws_DIST *dplay);

  /* Callable at any time--even before dws_Init or after dws_Kill */
	WORD dws_ENTRY dws_DGetRateFromDWD(BYTE dws_DIST *snd, WORD dws_DIST *rate);

  WORD dws_ENTRY dws_DDiscard(WORD soundnum);

  WORD dws_ENTRY dws_DDiscardAO(BYTE dws_DIST *snd);

  WORD dws_ENTRY dws_DClear(void);       /* global */

  WORD dws_ENTRY dws_DPause(void);       /* global */

  WORD dws_ENTRY dws_DUnPause(void);     /* global */

	WORD dws_ENTRY dws_DSuspend(void);

	WORD dws_ENTRY dws_DResume(void);


	WORD dws_ENTRY dws_3DSetWorld(dws_3DWORLD dws_DIST *dddworld);

	WORD dws_ENTRY dws_3DGetWorld(dws_3DWORLD dws_DIST *dddworld);


	WORD dws_ENTRY dws_DRecord(dws_DRECORD dws_DIST *drec);

	WORD dws_ENTRY dws_DStopRecord(void);

	WORD dws_ENTRY dws_DPauseRecord(void);

	WORD dws_ENTRY dws_DUnPauseRecord(void);


  /*
   . Converts a .WAV buffer to a .DWD buffer
   .
   . This function has two usages.  In the first, pass the wave pointer, the
   . wave length in *len and a NULL pointer for dwd.  The routine will return
   . the length of the buffer required to hold the resulting DWD in *len.  In
   . the second usage, allocate a buffer of the correct size and pass its
   . address in dwd.  Make sure you pass the wave pointer in wave and the size
   . of the wave in *len.  It will then perform the conversion, returning the
   . number of bytes used in *len.
  */
  WORD dws_ENTRY dws_WAV2DWD(BYTE dws_DIST *wave, DWORD dws_DIST *len,
                                      BYTE dws_DIST *dwd);

  /* The following 5 functions comprise the music functions of the STK. */
  WORD dws_ENTRY dws_MPlay(dws_MPLAY dws_DIST *mplay);

	WORD dws_ENTRY dws_MSetInfo(dws_MPLAY dws_DIST *mplay);

	WORD dws_ENTRY dws_MGetInfo(dws_MPLAY dws_DIST *mplay);

	WORD dws_ENTRY dws_MDiscard(WORD songnum);

  WORD dws_ENTRY dws_MClear(void);

  WORD dws_ENTRY dws_MPause(void);

	WORD dws_ENTRY dws_MUnPause(void);

	WORD dws_ENTRY dws_MSetTempo(WORD delta);


  /*
   . Converts a .MID buffer to a .DWM buffer
   .
   . This function has two usages.  In the first, pass the midi pointer, the
   . midi length in *len and a NULL pointer for dwm.  The routine will return
   . the length of the buffer required to hold the resulting DWM in *len.  In
   . the second usage, allocate a buffer of the correct size and pass its
   . address in dwm.  Make sure you pass the midi pointer in midi and the size
   . of the midi in *len.  It will then perform the conversion, returning the
   . number of bytes used in *len.
  */
  WORD dws_ENTRY dws_MID2DWM(BYTE dws_DIST *midi, DWORD dws_DIST *len,
                                      BYTE dws_DIST *dwm);


	WORD dws_ENTRY dws_CDGetInfo(dws_CDINFO dws_DIST *info);

	WORD dws_ENTRY dws_CDGetTrackTime(DWORD tracknum, DWORD dws_DIST *time);

	WORD dws_ENTRY dws_CDPlay(dws_CDPLAY dws_DIST *cdplay);

	WORD dws_ENTRY dws_CDStop(void);

	WORD dws_ENTRY dws_CDPause(void);

	WORD dws_ENTRY dws_CDUnPause(void);

	WORD dws_ENTRY dws_CDEject(void);

	WORD dws_ENTRY dws_CDUnEject(void);


  /*
   . If your 16-bit application can't yield CPU time to the system, it must
   . call this function periodically to prevent the sound from pausing.
   . This is not required for 32-bit applications, but the function is
   . supported for compatibility.
  */
  WORD dws_ENTRY dws_Update(void);        /* Affects all sounds */


  /*
   . Registers a DSP module with the ToolKit so that it may be used by
   . the application program during sound processing. Up to eight DSP
   . modules can be registered at a time.
  */
  WORD dws_ENTRY dws_DSPRegister(char dws_DIST *dspname, DWORD dws_DIST *dspmod);


  /*
   . Unregisters a DSP module with the ToolKit. Freed modules can be
   . reused by new modules with the use of dws_DSPRegister.
  */
  WORD dws_ENTRY dws_DSPUnregister(DWORD dspmod);




  #ifdef __cplusplus
    }
  #endif

/*****************************************************************************/



  #ifdef  dws_DWORDDEFINED
    #undef  DWORD
    #undef  dws_DWORDDEFINED
  #endif

  #ifdef  dws_WORDDEFINED
    #undef  WORD
    #undef  dws_WORDDEFINED
  #endif

  #ifdef  dws_BYTEDEFINED
    #undef  BYTE
    #undef  dws_BYTEDEFINED
  #endif



	/*lint --flb*/



#endif
