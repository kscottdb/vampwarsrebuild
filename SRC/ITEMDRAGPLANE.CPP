// ITEMDRAGPLANE.CPP -- the plane in which an inventory item will be dragged around on
//
// TW


#include "scimain.h"
#include "itemdragplane.h"
#include "intplane.h"
#include "gamemap.h"
#include "mapplane.h"
#include "sciprop.h"
#include "command.h"

ItemDragPlane::ItemDragPlane(int nPriority, int nItemBitmap, Unit *poUnit, int nItemIndex) 
 : sciPlane(pApp->pWindow, FALSE, nPriority)
{
	mpoUnit = poUnit;
	mnItemIndex = nItemIndex;

	vSetRect(0,0,640,480);
	mpoDragItem = new sciProp();
	mpoDragItem->vSetPlane(this);
	mpoDragItem->SetView(nItemBitmap);
	mpoDragItem->Init();
	mpoDragItem->Posn(GetCursorX(), GetCursorY());
	mpoDragItem->Show();
}

ItemDragPlane::~ItemDragPlane()
{
	gpoInterface->vRefreshInventory();
	delete mpoDragItem;
	mpoDragItem = NULL;
	mpoUnit = NULL;
}

void ItemDragPlane::Doit()
{
	sciPlane::Doit();
	mpoDragItem->Posn(GetCursorX(), GetCursorY());
}

Boolean ItemDragPlane::HandleEvent(sciEvent &oEvent)
{
	//mpoDragItem->Posn(oEvent.x, oEvent.y);
	switch (oEvent.type)
	{
	case MOUSE_UP:
		//localize to map plane
		gpoMap->mpoPlane->vGlobalToLocal(&oEvent.x, &oEvent.y);
		
		if (gpoMap->mpoPlane->OnMe(oEvent.x, oEvent.y))
		{
			vDebugOut ("On map:\n");
			Unit *poUnit = gpoMap->mpoPlane->poOnUnit(oEvent);
			MapNode *poNode = gpoMap->poDraggedItemNode(oEvent);
			
			if (poUnit)
			//item was released on some unit
			{
				
				if (poUnit->bIsLocalVampire())
				{
					//item was released on a local vampire so its ok to give
					Command *poCommand = new Command;
					poCommand->meType = CM_GIVE;
					poCommand->mbTargetUnit = TRUE;
					poCommand->mnUnit = poUnit->nGetID();
					poCommand->mnNewValue = mnItemIndex;
					game->poLocalPlayer()->vPostCommand(poCommand, FALSE);

					vDebugOut("on a local vampire\n");
				}

			}
			else
			{
				if (poNode)
				{
					Command *poCommand = new Command;
					poCommand->mnX = poNode->mnXPos;
					poCommand->mnY = poNode->mnYPos;
					poCommand->mnNewValue = mnItemIndex;
					poCommand->meType = CM_DROP;
					game->poLocalPlayer()->vPostCommand(poCommand, FALSE);
					vDebugOut("on clear node\n");
				}
			}

			//put back to normal
			gpoMap->mpoPlane->vLocalToGlobal(&oEvent.x, &oEvent.y);


		}
		else
		{
			gpoMap->mpoPlane->vLocalToGlobal(&oEvent.x, &oEvent.y);	//re-globalize
			gpoInterface->vGlobalToLocal(&oEvent.x, &oEvent.y);	//localize to interface

			Unit *poUnit = gpoInterface->poSwapItems(oEvent, mpoUnit);
			if (poUnit)
			{
				//item was released on the other dual portrait
				Command *poCommand = new Command;
				poCommand->meType = CM_GIVE;
				poCommand->mbTargetUnit = TRUE;
				poCommand->mnUnit = poUnit->nGetID();
				poCommand->mnNewValue = mnItemIndex;
				game->poLocalPlayer()->vPostCommand(poCommand, FALSE);
				vDebugOut("Swapping inventory\n");
			}

			
			//put back to normal
			gpoInterface->vLocalToGlobal(&oEvent.x, &oEvent.y);	

		}

		
		delete this;
		break;
	}
	
	//always claim the event until we release the mouse button and get disposed
	oEvent.claimed = TRUE;

	return oEvent.claimed;
}

