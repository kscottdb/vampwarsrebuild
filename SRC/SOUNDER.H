#ifndef _sounder
#define _sounder

#include "vsounder.h"

class Sounder : public VSounder
	{
	public:
		Sounder();
		~Sounder();
		void EndSound();
		void TurnSoundOff();
		void TurnSoundOn();

	protected:
		void PlayItNow();

		WAVEHDR		whWaveHdr;
		LPSTR		lpWaveData;
		HWAVEOUT	hWaveOut;
		BOOL		headerPrepared;
		BOOL		soundOn;
	};

#endif // _sounder
