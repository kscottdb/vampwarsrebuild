/////////////////////////////////////////////////////////////////////////////
// Project SIGS
//   Copyright � 1997. All Rights Reserved.
//
//     SUBSYSTEM:    SIGS
//     FILE:         sigswrap.cpp
//     AUTHOR:       Brick Baldwin
//
//     OVERVIEW
//     ========
//     Wrapper class for managing a SIGS session
//
//     Steps to use:
//     1) Construct an instance of the class:
//        "ASIGSSession *pSIGS = new ASIGSSession"
//
//     2) Call the Create() method of the class, which will return a
//        status flag indicating successful creation:
//
//        VALIDATE_DLL_EXIT eStatus = pSIGS->Create(hWndMain,
//                                                  "c:\\sierra\\lords2\\",
//                                                  "c:\\sierra\\lords2\\lords2.exe /switch",
//                                                  "Lords2");
//
//        Arguments are the HWND of the main application, installation path of
//        the game, command line for starting the game (used if SIGS updates
//        the game to a newer version available), and the SIGS server name.
//
//        VALIDATE_DLL_EXIT will be one of 3 values:
//        1) DLL_EXIT_CONTINUE_TO_CONNECT: This means the user has entered SIGS
//           and successfully connected to a game. Equivalent to an OK.
//        2) DLL_EXIT_FAILED_TO_UPDATE: This means the user has entered and then
//           exited SIGS without joining a game.  Equivalent to a cancel.
//        3) DLL_EXIT_FOR_SELF_UPDATE: This means that the user has connected
//           to a SIGS server and SIGS has determined that an upgrade is available
//           for the game.  If you receive this return code, you should IMMEDIATELY
//           perform any neccessary cleanup and SHUT DOWN with no user interaction.
//           SIGS will download the updates and relaunch the game.
//
//        Make sure you check the return status, this routine does not return
//        until the user has gotten into SIGS, chatted it up for awhile, and
//        actually entered a game, or exited SIGS.  This could take days.
//
//     3) Assuming Create() return TRUE, you have a live SIGS session and
//        commence to use any of the gametime methods provided.
//
//     4) When you are ready to quite, just call Close() and delete the SIGS object.
//
//#include <windows.h>
#include "os.h"
#include "sigswrap.h"

// -- Glow Balls ------------------------------------------------------------
static HANDLE ghSIGSGameStart;
ASIGSSession *ASIGSSession::m_pThis;

// Uncomment if you are using the debug heap (a damn nice thing)
//
//#ifdef _DEBUG
//#define new DEBUG_NEW
//#undef THIS_FILE
//static char THIS_FILE[] = __FILE__;
//#endif

/////////////////////////////////////////////////////////////////////////////
ASIGSSession::ASIGSSession()
{
    // Initialize public instance data
    m_bGameInProgress = FALSE;
    m_bSIGSConnected  = FALSE;
    m_bShuttingDown   = FALSE;
    m_bHost           = FALSE;

    // Initialize private instance data
    m_hSIGSThrd       = NULL;
    m_hwndGameApp     = NULL;
    m_pszServerName   = NULL;
    m_pszInstallPath  = NULL;
    m_pszRestartCmd   = NULL;
    m_hndDLL          = NULL;

    // Clear SIGS DLL proc addresses
    lpfnSNWValidate               = NULL;
    lpfnInitializeDLL             = NULL;
    lpfnCloseDll                  = NULL;
    lpfnCheckForAnyTCPMessage     = NULL;
    lpfnPeekForTCPMessage         = NULL;
    lpfnRecvTCPMessage            = NULL;
    lpfnCheckGameConnectStatus    = NULL;
    lpfnGameClose                 = NULL;
    lpfnGetNumberOfPlayers        = NULL;
    lpfnSendTCPMessage            = NULL;
    lpfnSendTCPPointMessage       = NULL;
    lpfnGetPlayerXName            = NULL;
    lpfnGetMyPlayerNumber         = NULL;
    lpfnGetGameOption             = NULL;
    lpfnTellMeGameMessReceived    = NULL;
    lpfnFreeGameMessageBuffer     = NULL;
    lpfnPingTime                  = NULL;
    lpfnTellMeSpecialChatReceived = NULL;
    lpfnTellMePlayerLeftGame      = NULL;
}

/////////////////////////////////////////////////////////////////////////////
VALIDATE_DLL_EXIT ASIGSSession::Create(HWND  hwndGameApp,
                                       LPSTR pszInstallPath,
                                       LPSTR pszRestartCmd,
                                       LPSTR pszServerName)
{
    m_hwndGameApp    = hwndGameApp;
    m_pszInstallPath = pszInstallPath;
    m_pszRestartCmd  = pszRestartCmd;
    m_pszServerName  = pszServerName;

    // Set "this" pointer for static functions (thread & callback)
    m_pThis = this;

    VALIDATE_DLL_EXIT eOpenStatus = Open();
    switch (eOpenStatus)
    {
        case DLL_EXIT_CONTINUE_TO_CONNECT:
        {
            // Into SIGS, wait for game start
            ghSIGSGameStart = ::CreateEvent(NULL, TRUE, FALSE, NULL);
            if (!NewGame())
            {
                eOpenStatus = DLL_EXIT_FAILED_TO_UPDATE;
            }
            break;
        }

        case DLL_EXIT_FAILED_TO_UPDATE:
        {
            m_bShuttingDown = TRUE;
            break;
        }

        case DLL_EXIT_FOR_SELF_UPDATE:
        {
            // Game is being updated - BAIL OUT NOW!
            break;
        }

        default:
        {
            // Things have gone awry
            ::MessageBox(m_hwndGameApp,
                         "Failure has occured with Internet play.",
                         "Failure",
                         MB_ICONERROR|MB_OK);
            break;
        }
    }
    return eOpenStatus;
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::NewGame()
{
    // Bring SIGS to the foreground
    // -- This has been done by exiting the game

    // We are going to wait here for the Captain to click start
    // (Gilligan meanwhile will be watching Ginger take a shower)
    while (::WaitForSingleObject(ghSIGSGameStart, 300) == WAIT_TIMEOUT)
    {
        // Take a break to pump any waiting messages
        MSG msg;
        while (::PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
        {
       		if (msg.message == WM_QUIT)
            {
                return FALSE;
            }
            if (::GetMessage(&msg,NULL,0,0))
            {
                ::TranslateMessage(&msg);
                ::DispatchMessage(&msg);
            }
        }
//::OutputDebugString("NewGame - waiting for event\n");
    }

    // Reset for potential next go-around
    ::ResetEvent(ghSIGSGameStart);
    ::ShowWindow(m_hwndGameApp,SW_RESTORE);
    ::SetForegroundWindow(m_hwndGameApp);
    ::SetFocus(m_hwndGameApp);

    // Return restart status
    if (m_bShuttingDown)
    {
        ::OutputDebugString("NewGame NAK\n");
        return FALSE;
    }
    else
    {
        ::OutputDebugString("NewGame ACK\n");
        return TRUE;
    }
}

/////////////////////////////////////////////////////////////////////////////
ASIGSSession::~ASIGSSession(void)
{
    if (m_bSIGSConnected)
    {
        Close();
    }
    m_pThis = NULL;
    return;
}

/////////////////////////////////////////////////////////////////////////////
VALIDATE_DLL_EXIT ASIGSSession::Open(void)
{
    BOOL bCheck = FALSE;

    char cpIPAdress[]="XXX.XXX.XXX.XXX";
    long lInPort = 0;
    long lOutPort = 0;
    VALIDATE_DLL_EXIT etRet = DLL_EXIT_FAILED_TO_UPDATE;

    m_hndDLL = ::LoadLibrary("SNWValid.dll");
    if (m_hndDLL != NULL)
    {
        lpfnSNWValidate = (lpfnSNWValidateFunc)GetProcAddress((HINSTANCE)m_hndDLL, "SNWValidate");
        if (lpfnSNWValidate != NULL)
        {
            // make the call to SNWValidate with your constructed parameters
            etRet = lpfnSNWValidate(m_pszServerName,
                                    cpIPAdress,
                                    &lInPort,
                                    &lOutPort,
                                    m_pszInstallPath,
                                    m_pszRestartCmd,
                                    m_hwndGameApp);
        }
        else
        {
            ::MessageBox(m_hwndGameApp,
                         "There was a problem Initializing SNWValid.dll.\nPlease check your installation before continuing.",
                         "Initialization Error",
                         MB_ICONERROR|MB_OK);
        }
        ::FreeLibrary(m_hndDLL);

        switch (etRet)
        {
            case DLL_EXIT_FOR_SELF_UPDATE:
            {
                // Exit your code now!
                // You are about to be updated
                // Make sure you have no dialogs to answer when exiting!!!
                break;
            }
            case DLL_EXIT_FAILED_TO_UPDATE:
            {
                bCheck = FALSE;
                break;
            }
            case DLL_EXIT_CONTINUE_TO_CONNECT:
            {
                bCheck = TRUE;
                break;
            }
            default:
                // We'll let you figure out what to do here.
                break;
        }
    }
    else
    {
        ::MessageBox(m_hwndGameApp,
                     "SNWValid.dll not found in Windows system folder.\nPlease check your installation before continuing.",
                     "File load error",
                     MB_ICONERROR|MB_OK);
    }

    if (etRet == DLL_EXIT_CONTINUE_TO_CONNECT)
    {
        m_hndDLL = NULL;

        // Load the SierraNW.dll once. Don't need to do this
        // for every function call
        m_hndDLL = ::LoadLibrary("SierraNW.dll");
        if (m_hndDLL != NULL)
        {
            if (InitFunctionPointers())
            {
                // make the call to SierraNW to initializee the DLL
                if (lpfnInitializeDLL(cpIPAdress, lInPort, lOutPort))
                {
                    etRet = DLL_EXIT_CONTINUE_TO_CONNECT;
                    // Start the ball rolling
                    DWORD dwTid;
                    m_hSIGSThrd = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)SIGSThread, 0, 0, &dwTid);

                    // Uncomment this to get on-demand message receipt notification
                    // Note: If on-demand is enabled, ReceiveMessages is disabled
                    //lpfnTellMeGameMessReceived(SIGS_CALLBACK_FUNCTION,(MESS_RECEIVED_PROC)MsgRcv, NULL, NULL);

                }
                else
                {
                    ::FreeLibrary(m_hndDLL);
                    m_hndDLL = NULL;
                    etRet = DLL_EXIT_FAILED_TO_UPDATE;
                    bCheck = FALSE;
                }
            }
            else
            {
                bCheck = FALSE;
                etRet =  DLL_EXIT_FAILED_TO_UPDATE;
                ::FreeLibrary(m_hndDLL);
                ::MessageBox(m_hwndGameApp,
                             "There was a problem Initializing SierraNW.dll.\nPlease check your installation before continuing.",
                             "Initialization Error", MB_ICONERROR|MB_OK);
            }
        }
        else
        {
            ::MessageBox(m_hwndGameApp,
                         "SierraNW.dll not found in Window system folder.\nPlease check your installation before continuing.",
                         "File load error",
                         MB_ICONERROR|MB_OK);

            etRet = DLL_EXIT_FAILED_TO_UPDATE;
        }
    }

    m_bSIGSConnected = bCheck;

    return etRet;
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::InitFunctionPointers(void)
{
    lpfnInitializeDLL             = (lpfnInitializeDLLFunc)             ::GetProcAddress(m_hndDLL, "InitializeDLL");
    lpfnCloseDll                  = (lpfnCloseDLLFunc)                  ::GetProcAddress(m_hndDLL, "CloseDLL");
    lpfnCheckGameConnectStatus    = (lpfnCheckGameConnectStatusFunc)    ::GetProcAddress(m_hndDLL, "CheckGameConnectStatus");
    lpfnGameClose                 = (lpfnGameCloseFunc)                 ::GetProcAddress(m_hndDLL, "GameClose");
    lpfnCheckForAnyTCPMessage     = (lpfnCheckForAnyTCPMessageFunc)     ::GetProcAddress(m_hndDLL, "CheckForAnyTCPMessage");
    lpfnPeekForTCPMessage         = (lpfnPeekForTCPMessageFunc)         ::GetProcAddress(m_hndDLL, "PeekForTCPMessage");
    lpfnGetNumberOfPlayers        = (lpfnGetNumberOfPlayersFunc)        ::GetProcAddress(m_hndDLL, "GetNumberOfPlayers");
    lpfnSendTCPMessage            = (lpfnSendTCPMessageFunc)            ::GetProcAddress(m_hndDLL, "SendTCPMessage");
    lpfnSendTCPPointMessage       = (lpfnSendTCPPointMessageFunc)       ::GetProcAddress(m_hndDLL, "SendTCPPointMessage");
    lpfnRecvTCPMessage            = (lpfnRecvTCPMessageFunc)            ::GetProcAddress(m_hndDLL, "RecvTCPMessage");
    lpfnGetPlayerXName            = (lpfnGetPlayerXNameFunc)            ::GetProcAddress(m_hndDLL, "GetPlayerXName");
    lpfnGetMyPlayerNumber         = (lpfnGetMyPlayerNumberFunc)         ::GetProcAddress(m_hndDLL, "GetMyPlayerNumber");
    lpfnGetGameOption             = (lpfnGetGameOptionFunc)             ::GetProcAddress(m_hndDLL, "GetGameOption");
    lpfnTellMeGameMessReceived    = (lpfnTellMeGameMessReceivedFunc)    ::GetProcAddress(m_hndDLL, "TellMeGameMessReceived");
    lpfnFreeGameMessageBuffer     = (lpfnFreeGameMessageBufferFunc)     ::GetProcAddress(m_hndDLL, "FreeGameMessageBuffer");
    lpfnPingTime                  = (lpfnPingTimeFunc)                  ::GetProcAddress(m_hndDLL, "PingTime");
    lpfnTellMeSpecialChatReceived = (lpfnTellMeSpecialChatReceivedFunc) ::GetProcAddress(m_hndDLL, "TellMeSpecialChatReceived");
    lpfnTellMePlayerLeftGame      = (lpfnTellMePlayerLeftGameFunc)      ::GetProcAddress(m_hndDLL, "TellMePlayerLeftGame");

    // Sanity
    if (lpfnInitializeDLL          &&
        lpfnCloseDll               &&
        lpfnCheckGameConnectStatus &&
        lpfnGameClose              &&
        lpfnCheckForAnyTCPMessage  &&
        lpfnPeekForTCPMessage      &&
        lpfnGetNumberOfPlayers     &&
        lpfnSendTCPMessage         &&
        lpfnSendTCPPointMessage    &&
        lpfnRecvTCPMessage         &&
        lpfnGetPlayerXName         &&
        lpfnGetMyPlayerNumber      &&
        lpfnGetGameOption          &&
        lpfnTellMeGameMessReceived &&
        lpfnFreeGameMessageBuffer  &&
        lpfnPingTime               &&
        lpfnTellMePlayerLeftGame   &&
        lpfnTellMeSpecialChatReceived)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::Close(void)
{
    // Let everything know we are out of here
    ::OutputDebugString("ASIGSSession::Close\n");
    m_bSIGSConnected = FALSE;

    // Unregister the call back
    lpfnTellMeGameMessReceived(SIGS_DEFAULT_QUERY,NULL,(HWND) NULL, NULL);

    if (m_hSIGSThrd)
    {
        m_bShuttingDown = TRUE;
        EndThread(&m_hSIGSThrd);
    }

    if (lpfnCloseDll != NULL)
    {
        // Make the call to SierraNW to close the DLL
        lpfnCloseDll();
        ::OutputDebugString("Unloading SIGS DLL\n");
        ::FreeLibrary(m_hndDLL);
        m_hndDLL = NULL;
        m_bGameInProgress = FALSE;
    }
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
void ASIGSSession::ExitGame(void)
{
    lpfnGameClose();
    m_bGameInProgress = FALSE;
    m_bHost = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// This is an indication that the player has clicked start game
BOOL ASIGSSession::IsGameReady()
{
    BOOL bCheck = FALSE;
    // If the DLL was unloaded than things are over.
    if (!m_hndDLL || !m_bSIGSConnected)
    {
        return bCheck;
    }

    if (lpfnCheckGameConnectStatus != NULL)
    {
        int iRet = lpfnCheckGameConnectStatus();
        if (iRet == _GAME_STAT_SUCCESS)
        {
            bCheck = TRUE;
            m_bGameInProgress = TRUE;
        }
        else if (m_bGameInProgress) // the game was closed
        {
            m_bGameInProgress = FALSE;
        }
    }
    return bCheck;
}

/////////////////////////////////////////////////////////////////////////////
int ASIGSSession::ReceiveMessage(char *cMsg, UINT nMaxSize, UINT *pnID)
{
    // Set default output
    *cMsg   = 0;
    *pnID   = 0;

    // If we have the ProcAddress to receive messages
    if (lpfnCheckForAnyTCPMessage)
    {
        // If there is a message queued (this routine returns 0=TRUE/-1=FALSE ?!?!)
        if (lpfnCheckForAnyTCPMessage() == 0)
        {
            // Check for messages from each player
            int iPlayers = GetNumberOfPlayers();
            for (int iPlayer = 0 ; iPlayer <= iPlayers ; iPlayer++)
            {
                int iMsgSize = lpfnPeekForTCPMessage((LPBYTE)cMsg, nMaxSize, iPlayer);
                if (iMsgSize > 0)
                {
                    // Make sure I do a size MAX_SIGS_MSG_SIZE check for sanity
                    if ((UINT)iMsgSize > nMaxSize)
                    {
                        // NAK as failure has occured
                        ::DebugBreak();//ASSERT
                        return 0;
                    }

                    iMsgSize = lpfnRecvTCPMessage((LPBYTE)cMsg, iMsgSize, iPlayer);
                    if (iMsgSize <= 0)
                    {
                        // NAK on bad message
                        ::DebugBreak();//ASSERT
                        return 0;
                    }
                    else
                    {
                        *pnID = iPlayer;
                        return iMsgSize;
                    }
                }
            }
        }
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::SendMessageAll(char *cMsg, int iSize)
{
    return (BOOL)lpfnSendTCPMessage((BYTE *)cMsg, (unsigned short)iSize);
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::SendPlayerMessage(int iPlayer, char *cMsg, int iSize)
{
    return (BOOL)lpfnSendTCPPointMessage(iPlayer, (BYTE *)cMsg, (unsigned short)iSize);
}

/////////////////////////////////////////////////////////////////////////////
void ASIGSSession::FreeMessageBuffer(void)
{
    lpfnFreeGameMessageBuffer();
}

/////////////////////////////////////////////////////////////////////////////
int ASIGSSession::PingGameServer()
{
    return lpfnPingTime(PING_GAME_SERVER,-1);
}

/////////////////////////////////////////////////////////////////////////////
void ASIGSSession::HookSpecialChat(SPECIAL_CHAT_PROC pfn)
{
    lpfnTellMeSpecialChatReceived(SIGS_CALLBACK_FUNCTION,pfn,NULL,0);
}

/////////////////////////////////////////////////////////////////////////////
void ASIGSSession::HookPlayerLeave(PLAYER_STATUS_PROC pfn)
{
    lpfnTellMePlayerLeftGame(SIGS_CALLBACK_FUNCTION,pfn,NULL,0);
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::IsConnected(void)
{
    BOOL bCheck = TRUE;

    if (!m_hndDLL || !m_bSIGSConnected)
    {
        return FALSE;
    }

    if (lpfnCheckGameConnectStatus != NULL)
    {
        // Make the call to SierraNW to close the DLL
        int iRet = lpfnCheckGameConnectStatus();
        if (iRet == _GAME_STAT_FAILURE)
        {
            bCheck = FALSE;
            m_bSIGSConnected = FALSE;
        }
        else
        {
            m_bSIGSConnected = TRUE;
        }

    }
    return bCheck;
}

/////////////////////////////////////////////////////////////////////////////
int ASIGSSession::GetNumberOfPlayers(void)
{
    return lpfnGetNumberOfPlayers(NULL);
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::IsCaptain()
{
    if (lpfnGetMyPlayerNumber() == 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/////////////////////////////////////////////////////////////////////////////
int ASIGSSession::GetMyNumber()
{
    return lpfnGetMyPlayerNumber();
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::GetMyName(LPSTR pszName, UINT nSize)
{
    char szTemp[MAX_SIGS_MSG_SIZE];
    lpfnGetPlayerXName(szTemp, (unsigned short)lpfnGetMyPlayerNumber());
    strncpy(pszName, szTemp, nSize);
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::GetGameOption(char *cOption, char *cVal, unsigned int iSize)
{
    // 100 is the max size in the data base
    if (iSize > MAX_GAME_OP_SIZE)
    {
       return FALSE;
    }

    char cTemp[MAX_GAME_OP_SIZE];
    BOOL bRet = lpfnGetGameOption(cOption, cTemp);

    if (bRet)
    {
        strncpy(cVal, cTemp, iSize);
    }
    else
    {
        cVal[0] = NULL;
    }

    return bRet;
}

/////////////////////////////////////////////////////////////////////////////
BOOL ASIGSSession::EndThread(HANDLE *hdThread)
{
    // Give the thread up to 8 seconds to end
    DWORD dwExitCode = ::WaitForSingleObject(*hdThread, 8000);

    if (dwExitCode == WAIT_OBJECT_0)
    {
        ::OutputDebugString("SIGSEndThread thread terminated AOK\n");
    }
    else if (dwExitCode == WAIT_TIMEOUT)
    {
        ::OutputDebugString("SIGSEndThread thread failed to terminate\n");
    }
    else if (dwExitCode == WAIT_ABANDONED)
    {
        ::OutputDebugString("SIGSEndThread thread ABANDONED\n");
    }
    else if (dwExitCode == WAIT_FAILED)
    {
        ::OutputDebugString("SIGSEndThread thread FAILED Wait\n");

        LPVOID lpMsgBuf = NULL;

        ::FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
            NULL,
            GetLastError(),
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
            (LPTSTR) &lpMsgBuf,
            0,
            NULL
        );

        // Display the string.
        ::OutputDebugString((LPSTR)lpMsgBuf);

        // Free the buffer.
        ::LocalFree( lpMsgBuf );
    }


    if (::GetExitCodeThread(*hdThread, &dwExitCode))
    {
        if (dwExitCode == 0)
        {
            // Free the thread handle
            ::OutputDebugString("SIGSEndThread Free Thread Resource\n");
            ::CloseHandle(*hdThread);
        }
        else
        {
            ::OutputDebugString("SIGSEndThread Forced thread termination\n");
            ::TerminateThread(hdThread, (DWORD)-2);
            ::CloseHandle(*hdThread);
        }
    }

    *hdThread = NULL;
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// my call-back for all messages from the other games
//
//void SIGSCALLBACK my_function (char *Msg, sMessInfo MsgInfo)
void ASIGSSession::MsgRcv (char *Msg, sMessInfo MsgInfo)
{
    m_pThis->FreeMessageBuffer();
}

/////////////////////////////////////////////////////////////////////////////
// This is a separate thread launched by Open() to monitor SIGS status
HRESULT ASIGSSession::SIGSThread()
{
    ::OutputDebugString("MonitorThread-Birth\n");
    // Give them a chance to click the start game button in SIGS
    while (!m_pThis->m_bShuttingDown)
    {
        // Make sure the user did not do an exit
        if (!m_pThis->IsConnected())
        {
            ::OutputDebugString("MonitorThread-Disconnect\n");
            m_pThis->m_bShuttingDown = TRUE;
            ::SetEvent(ghSIGSGameStart);
            break;
        }

        if (m_pThis->IsGameReady())
        {
            // Game is ready and lets give back the focus to golf.
            // Let the world know there is a game
            ::OutputDebugString("MonitorThread-GameStart\n");
            ::SetEvent(ghSIGSGameStart);

            // If they are in a game then we start watching for messages.
            while (m_pThis->IsGameReady())
            {
                //if (!m_pThis->IsConnected())
                //{
                //    m_pThis->m_bShuttingDown = TRUE;
                //    SetEvent(ghSIGSGameStart);
                //    break;
                //}
                Sleep(350);
            }
            ::OutputDebugString("MonitorThread-GameEnd\n");
        }
        ::Sleep(350);
    }
    ::OutputDebugString("MonitorThread-Death\n");
    ::CloseHandle(ghSIGSGameStart);
    return 0;
}
