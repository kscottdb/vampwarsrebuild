#include "os.h"
#include "texture.h"
#include "framebuf.h"
#include "vbitmap.h"

void SwapBytes(WORD * wData);
void SwapBytes(DWORD * dwData);
void FileReadBytes(FILE * file, BYTE * buffer, int count = 1);
void FileReadWords(FILE * file, WORD * buffer, int count = 1);
void FileReadDWords(FILE * file, DWORD * buffer, int count = 1);
void FileWriteBytes(FILE * file, BYTE * buffer, int count = 1);
void FileWriteWords(FILE * file, WORD * buffer, int count = 1);
void FileWriteDWords(FILE * file, DWORD * buffer, int count = 1);
void MacSwapBytes(WORD * wData);
void MacSwapBytes(DWORD * dwData);

int IsTransparent(BYTE * sourceRow, BYTE * alphaRow);
int IsOpaque(BYTE * sourceRow, BYTE * alphaRow);
int IsAlpha(BYTE * sourceRow, BYTE * alphaRow);
int IsRLE(BYTE * sourceRow, BYTE * alphaRow, BYTE * endOfRow);
int fsize(FILE * file);

int IsTransparent(BYTE * sourceRow, BYTE * alphaRow)
{
	if (
		((alphaRow == NULL) && (*sourceRow == 0)) ||
		((alphaRow != NULL) && (*alphaRow == 0))
	)
		return TRUE;
	else
		return FALSE;
}

int IsOpaque(BYTE * sourceRow, BYTE * alphaRow)
{
	if (
		((alphaRow == NULL) && (*sourceRow != 0)) ||
		((alphaRow != NULL) && (*alphaRow == 255))
	)
		return TRUE;
	else
		return FALSE;
}

int IsAlpha(BYTE *, BYTE * alphaRow)
{
	if ((alphaRow != NULL) && (*alphaRow != 0) && (*alphaRow != 255))
		return TRUE;
	else
		return FALSE;
}

int IsRLE(BYTE * sourceRow, BYTE * alphaRow, BYTE * endOfRow)
{
	if (
		((sourceRow + 2) <= endOfRow) &&
		(*sourceRow == (*(sourceRow + 1))) &&
		(*sourceRow == (*(sourceRow + 2)))
	) {
		if (
			(alphaRow != NULL) &&
			IsOpaque(sourceRow, alphaRow) &&
			IsOpaque(sourceRow + 1, alphaRow + 1) &&
			IsOpaque(sourceRow + 2, alphaRow + 2)
		)
			return TRUE;
		else if (
				 (alphaRow == NULL) &&
				 IsOpaque(sourceRow, NULL) &&
				 IsOpaque(sourceRow + 1, NULL) &&
				 IsOpaque(sourceRow + 2, NULL)
			)
			return TRUE;
		else
			return FALSE;
	} else
		return FALSE;
}

BYTE *TextureCompressRow(BYTE * sourceRow, int nLength, int *lengthOfRow, BYTE * alphaRow)
{
	static BYTE tempRow[1024];
	BYTE *endOfSourceRow;
	BYTE *outRow;
	BYTE *tempOut;
	BYTE *compressedRow;
	int i;

	*lengthOfRow = 0;
	endOfSourceRow = sourceRow + nLength;
	outRow = &(tempRow[0]);

	while (sourceRow < endOfSourceRow) {
		i = 1;
		if (IsTransparent(sourceRow, alphaRow)) {
			++sourceRow;
			if (alphaRow)
				++alphaRow;
			while (IsTransparent(sourceRow, alphaRow) && (sourceRow < endOfSourceRow)) {
				++sourceRow;
				if (alphaRow)
					++alphaRow;
				++i;
			}
			*outRow = TRANSPARENT_FLAG;
			++outRow;
			*(short *) outRow = i;
			MacSwapBytes((WORD *) outRow);
			outRow += sizeof(short);
		} else if (IsAlpha(sourceRow, alphaRow)) {
			*outRow = ALPHA_FLAG;
			++outRow;
			tempOut = outRow;
			outRow += sizeof(short);
			*outRow = *sourceRow;
			++outRow;
			++sourceRow;
			if (alphaRow)
				++alphaRow;
			*outRow = *alphaRow;
			++outRow;
			while (IsAlpha(sourceRow, alphaRow) && (sourceRow < endOfSourceRow)) {
				*outRow = *sourceRow;
				++outRow;
				++sourceRow;
				if (alphaRow)
					++alphaRow;
				++i;
				*outRow = *alphaRow;
				++outRow;
			}
			*tempOut = i;
			*(short *) tempOut = i;
			MacSwapBytes((WORD *) tempOut);
			tempOut += sizeof(short);
		} else if (IsOpaque(sourceRow, alphaRow) && !IsRLE(sourceRow, alphaRow, endOfSourceRow)) {
			*outRow = OPAQUE_FLAG;
			++outRow;
			tempOut = outRow;
			outRow += sizeof(short);
			*outRow = *sourceRow;
			++outRow;
			++sourceRow;
			if (alphaRow)
				++alphaRow;
			while (IsOpaque(sourceRow, alphaRow) && (sourceRow < endOfSourceRow)
				   && !IsRLE(sourceRow, alphaRow, endOfSourceRow)) {
				*outRow = *sourceRow;
				++outRow;
				++sourceRow;
				if (alphaRow)
					++alphaRow;
				++i;
			}
			*tempOut = i;
			*(short *) tempOut = i;
			MacSwapBytes((WORD *) tempOut);
			tempOut += sizeof(short);
		} else {
			// RLE
			*outRow = RLE_FLAG;
			++outRow;
			tempOut = sourceRow;
			++sourceRow;
			if (alphaRow)
				++alphaRow;
			while ((*sourceRow == *tempOut) && (sourceRow < endOfSourceRow) && !IsAlpha(sourceRow, alphaRow)) {
				++sourceRow;
				if (alphaRow)
					++alphaRow;
				++i;
			}
			*(short *) outRow = i;
			MacSwapBytes((WORD *) outRow);
			outRow += sizeof(short);
			*outRow = *tempOut;
			++outRow;
		}
	}
	*lengthOfRow = outRow - &(tempRow[0]);
	compressedRow = new BYTE[*lengthOfRow];
	memcpy(compressedRow, tempRow, *lengthOfRow);
	return compressedRow;
}


void FASTCALL TextureRenderRow(BYTE * pjDestRow, BYTE * pjCompressedRow, 
							   int nStart, int nLength)
{
	int nSize;
	BYTE jFlags;

	/*
	int i,
	 j;
	BYTE a;
	BYTE f,
	 b;
	*/

	jFlags = *pjCompressedRow;
	++pjCompressedRow;
	nSize = *(short *) pjCompressedRow;
	pjCompressedRow += sizeof(short);

	// skip runs until we come to one that
	// overlaps start position
	while (nStart >= nSize) {
		nStart -= nSize;
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			break;
		case OPAQUE_FLAG:
			pjCompressedRow += nSize;
			break;
		case ALPHA_FLAG:
			assert(0); // no alpha for now -- DJM TBD
#if 0
			pjCompressedRow += nSize * 2;
#endif
			break;

		case RLE_FLAG:
			++pjCompressedRow;
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
		jFlags = *pjCompressedRow;
		++pjCompressedRow;
		nSize = *(short *) pjCompressedRow;
		pjCompressedRow += sizeof(short);
	}

	// nStart now contains just the amount of the 
	// run to skip, so reduce size by that amount
	//
	nSize -= nStart;

	// move up in the compressed row, only on the first run
	//
	switch (jFlags) {
	case OPAQUE_FLAG:
		pjCompressedRow += nStart;
		break;

	case ALPHA_FLAG:
		assert(0); // no alpha for now -- DJM TBD
		pjCompressedRow += nStart * 2;
		break;
	}


	while (nLength) {
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				//nStart = 0;
				pjDestRow += nSize;
				nLength -= nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				nLength = 0;
			}
			break;
		case OPAQUE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				//nStart = 0;
				memcpy(pjDestRow, pjCompressedRow, nSize);
				pjDestRow += nSize;
				nLength -= nSize;
				pjCompressedRow += nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				memcpy(pjDestRow, pjCompressedRow, nLength);
				nLength = 0;
			}
			break;
		case ALPHA_FLAG:
			assert(0); // no alpha for now -- DJM TBD
#if 0
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				//nStart = 0;
				// memcpy(pjDestRow, pjCompressedRow, nSize);
				for (i = 0; i < nSize; ++i) {
					f = *pjCompressedRow;
					++pjCompressedRow;
					a = *pjCompressedRow;
					++pjCompressedRow;
					b = *pjDestRow;
					if (colors != NULL) {
						f = PaletteCalculateAlpha(colors, f, b, a);
					} else {
						a = (255 - a) >> 6;
						for (j = 0; j < a; ++j) {
							f = alpha[f][b];
						}
					}
					*pjDestRow = f;
					++pjDestRow;
				}
				nLength -= nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				// memcpy(pjDestRow, pjCompressedRow, nLength);
				for (i = 0; i < nLength; ++i) {
					f = *pjCompressedRow;
					++pjCompressedRow;
					a = *pjCompressedRow;
					++pjCompressedRow;
					b = *pjDestRow;
					if (colors != NULL) {
						f = PaletteCalculateAlpha(colors, f, b, a);
					} else {
						a = (255 - a) >> 6;
						for (j = 0; j < a; ++j) {
							f = alpha[f][b];
						}
					}
					*pjDestRow = f;
					++pjDestRow;
				}
				nLength = 0;
			}
#endif
			break;
		case RLE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				//nStart = 0;
				memset(pjDestRow, *pjCompressedRow, nSize);
				++pjCompressedRow;
				pjDestRow += nSize;
				nLength -= nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				memset(pjDestRow, *pjCompressedRow, nLength);
				nLength = 0;
			}
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
	}
}

#pragma optimize( "agt", on )

// TEMP -- DJM
//#pragma inline_depth(0)

extern WORD *gpwRandPixels;

void FASTCALL TextureRenderRow16(WORD * pwDestRow, BYTE * pjCompressedRow, int nStart, int nLength, WORD * pwPalette)
{
	int nSize;
	BYTE jFlags;
	//int i,
	// j;
	//WORD a, b;
	//BYTE f;
	Blender *poBlender = gpoBlender;

	jFlags = *pjCompressedRow;
	++pjCompressedRow;
	nSize = *(short *) pjCompressedRow;
	pjCompressedRow += sizeof(short);

	// skip runs until we come to one that
	// overlaps start position
	while (nStart >= nSize) {
		nStart -= nSize;
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			break;
		case OPAQUE_FLAG:
			pjCompressedRow += nSize;
			break;
		case ALPHA_FLAG:
			pjCompressedRow += nSize * 2;
			break;
		case RLE_FLAG:
			++pjCompressedRow;
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
		jFlags = *pjCompressedRow;
		++pjCompressedRow;
		nSize = *(short *) pjCompressedRow;
		pjCompressedRow += sizeof(short);
	}



	// nStart now contains just the amount of the 
	// run to skip, so reduce size by that amount
	//
	nSize -= nStart;

	// move up in the compressed row, only on the first run
	//
	switch (jFlags) {
	case OPAQUE_FLAG:
		pjCompressedRow += nStart;
		break;

	case ALPHA_FLAG:
		pjCompressedRow += nStart * 2;
		break;
	}


	while (nLength) {
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				pwDestRow += nSize;
				nLength -= nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				nLength = 0;
			}
			break;
		case OPAQUE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.

				// TEMP TEST -- DJM
				vCopyPixelsPalette16(pwDestRow, pjCompressedRow, nSize, pwPalette);
				//vCopyPixels16(pwDestRow, gpwRandPixels, nSize);
				pwDestRow += nSize;
				nLength -= nSize;
				pjCompressedRow += nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				// TEMP TEST -- DJM
				vCopyPixelsPalette16(pwDestRow, pjCompressedRow, nLength, pwPalette);
				//vCopyPixels16(pwDestRow, gpwRandPixels, nLength);
				nLength = 0;
			}
			break;
		case ALPHA_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				nLength -= nSize;
				while (nSize--) {
					WORD wFore = pwPalette[*pjCompressedRow];
					++pjCompressedRow;
					BYTE jAlpha = *pjCompressedRow;
					++pjCompressedRow;
					WORD wBack = *pwDestRow;

					*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jAlpha));

					++pwDestRow;
				}
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				// memcpy(pwDestRow, pjCompressedRow, nLength);
				while (nLength--) {
					WORD wFore = pwPalette[*pjCompressedRow];
					++pjCompressedRow;
					BYTE jAlpha = *pjCompressedRow;
					++pjCompressedRow;
					WORD wBack = *pwDestRow;

					*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jAlpha));

					++pwDestRow;
				}
				nLength = 0;
			}
			break;
		case RLE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				vFillPixels16(pwDestRow, pwPalette[*pjCompressedRow], nSize);
				++pjCompressedRow;

				pwDestRow += nSize;
				nLength -= nSize;

				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				vFillPixels16(pwDestRow, pwPalette[*pjCompressedRow], nLength);
				nLength = 0;
			}
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
	}
}


void FASTCALL TextureRenderRowFlip16(WORD * pwDestRow, BYTE * pjCompressedRow, int nStart, int nLength, WORD * pwPalette)
{
	int nSize;
	BYTE jFlags;
	//int i,
	// j;
	//WORD a, b;
	//BYTE f;
	Blender *poBlender = gpoBlender;

	jFlags = *pjCompressedRow;
	++pjCompressedRow;
	nSize = *(short *) pjCompressedRow;
	pjCompressedRow += sizeof(short);

	// skip runs until we come to one that
	// overlaps start position
	while (nStart >= nSize) {
		nStart -= nSize;
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			break;
		case OPAQUE_FLAG:
			pjCompressedRow += nSize;
			break;
		case ALPHA_FLAG:
			pjCompressedRow += nSize * 2;
			break;
		case RLE_FLAG:
			++pjCompressedRow;
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
		jFlags = *pjCompressedRow;
		++pjCompressedRow;
		nSize = *(short *) pjCompressedRow;
		pjCompressedRow += sizeof(short);
	}



	// nStart now contains just the amount of the 
	// run to skip, so reduce size by that amount
	//
	nSize -= nStart;

	// move up in the compressed row, only on the first run
	//
	switch (jFlags) {
	case OPAQUE_FLAG:
		pjCompressedRow += nStart;
		break;

	case ALPHA_FLAG:
		pjCompressedRow += nStart * 2;
		break;
	}


	while (nLength) {
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				pwDestRow -= nSize;
				nLength -= nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				nLength = 0;
			}
			break;
		case OPAQUE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.

				vCopyPixelsPaletteRev16(pwDestRow, pjCompressedRow, nSize, pwPalette);
				pwDestRow -= nSize;

				nLength -= nSize;
				pjCompressedRow += nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				vCopyPixelsPaletteRev16(pwDestRow, pjCompressedRow, nLength, pwPalette);
				nLength = 0;
			}
			break;
		case ALPHA_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				nLength -= nSize;
				while (nSize--) {
					WORD wFore = pwPalette[*pjCompressedRow];
					++pjCompressedRow;
					BYTE jAlpha = *pjCompressedRow;
					++pjCompressedRow;
					WORD wBack = *pwDestRow;

					*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jAlpha));

					--pwDestRow;
				}
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				// memcpy(pwDestRow, pjCompressedRow, nLength);
				while (nLength--) {
					WORD wFore = pwPalette[*pjCompressedRow];
					++pjCompressedRow;
					BYTE jAlpha = *pjCompressedRow;
					++pjCompressedRow;
					WORD wBack = *pwDestRow;

					*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jAlpha));

					--pwDestRow;
				}
				nLength = 0;
			}
			break;
		case RLE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				vFillPixelsRev16(pwDestRow, pwPalette[*pjCompressedRow], nSize);
				pwDestRow -= nSize;

				++pjCompressedRow;

				nLength -= nSize;

				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				vFillPixelsRev16(pwDestRow, pwPalette[*pjCompressedRow], nLength);
				nLength = 0;
			}
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
	}
}


#pragma optimize( "agt", on )

void FASTCALL TextureRenderRowAlpha16(WORD * pwDestRow, BYTE * pjCompressedRow, int nStart, int nLength, WORD * pwPalette, BYTE jAlpha, BOOL bFlip)
{
	int nSize;
	BYTE jFlags;
	//int i,
	// j;
	//WORD a, b;
	//BYTE f;
	Blender *poBlender = gpoBlender;

	jFlags = *pjCompressedRow;
	++pjCompressedRow;
	nSize = *(short *) pjCompressedRow;
	pjCompressedRow += sizeof(short);

	// skip runs until we come to one that
	// overlaps start position
	while (nStart >= nSize) {
		nStart -= nSize;
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			break;
		case OPAQUE_FLAG:
			pjCompressedRow += nSize;
			break;
		case ALPHA_FLAG:
			pjCompressedRow += nSize * 2;
			break;
		case RLE_FLAG:
			++pjCompressedRow;
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
		jFlags = *pjCompressedRow;
		++pjCompressedRow;
		nSize = *(short *) pjCompressedRow;
		pjCompressedRow += sizeof(short);
	}



	// nStart now contains just the amount of the 
	// run to skip, so reduce size by that amount
	//
	nSize -= nStart;

	// move up in the compressed row, only on the first run
	//
	switch (jFlags) {
	case OPAQUE_FLAG:
		pjCompressedRow += nStart;
		break;

	case ALPHA_FLAG:
		pjCompressedRow += nStart * 2;
		break;
	}


	while (nLength) {
		switch (jFlags) {
		case TRANSPARENT_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				if (bFlip) {
					pwDestRow -= nSize;
				} else {
					pwDestRow += nSize;
				}
				nLength -= nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				nLength = 0;
			}
			break;
		case OPAQUE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				if (bFlip) {
					vCopyPixelsPaletteAlphaRev16(pwDestRow, pjCompressedRow, nSize, pwPalette, jAlpha);
					pwDestRow -= nSize;
				} else {
					vCopyPixelsPaletteAlpha16(pwDestRow, pjCompressedRow, nSize, pwPalette, jAlpha);
					pwDestRow += nSize;
				}
				nLength -= nSize;
				pjCompressedRow += nSize;
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				if (bFlip) {
					vCopyPixelsPaletteAlphaRev16(pwDestRow, pjCompressedRow, nLength, pwPalette, jAlpha);
				} else {
					vCopyPixelsPaletteAlpha16(pwDestRow, pjCompressedRow, nLength, pwPalette, jAlpha);
				}
				nLength = 0;
			}
			break;
		case ALPHA_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				nLength -= nSize;
				if (bFlip) {
					while (nSize--) {
						WORD wFore = pwPalette[*pjCompressedRow];
						++pjCompressedRow;
						BYTE jLocalAlpha = (BYTE)(((WORD)*pjCompressedRow * (WORD)jAlpha) >> 8);
						++pjCompressedRow;
						WORD wBack = *pwDestRow;

						*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jLocalAlpha));

						--pwDestRow;
					}
				} else {
					while (nSize--) {
						WORD wFore = pwPalette[*pjCompressedRow];
						++pjCompressedRow;
						BYTE jLocalAlpha = (BYTE)(((WORD)*pjCompressedRow * (WORD)jAlpha) >> 8);
						++pjCompressedRow;
						WORD wBack = *pwDestRow;

						*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jLocalAlpha));

						++pwDestRow;
					}
				}
				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				// memcpy(pwDestRow, pjCompressedRow, nLength);
				if (bFlip) {
					while (nLength--) {
						WORD wFore = pwPalette[*pjCompressedRow];
						++pjCompressedRow;
						BYTE jLocalAlpha = (BYTE)(((WORD)*pjCompressedRow * (WORD)jAlpha) >> 8);
						++pjCompressedRow;
						WORD wBack = *pwDestRow;

						*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jLocalAlpha));

						--pwDestRow;
					}
				} else {
					while (nLength--) {
						WORD wFore = pwPalette[*pjCompressedRow];
						++pjCompressedRow;
						BYTE jLocalAlpha = (BYTE)(((WORD)*pjCompressedRow * (WORD)jAlpha) >> 8);
						++pjCompressedRow;
						WORD wBack = *pwDestRow;

						*pwDestRow = (WORD)(poBlender->dwCalcAlpha(wBack, wFore, jLocalAlpha));

						++pwDestRow;
					}
				}
				nLength = 0;
			}
			break;
		case RLE_FLAG:
			if (nLength > nSize) {
				// This run of pixels isn't enough.
				if (bFlip) {
					vFillPixelsAlphaRev16(pwDestRow, pwPalette[*pjCompressedRow], nSize, jAlpha);
					pwDestRow -= nSize;
				} else {
					vFillPixelsAlpha16(pwDestRow, pwPalette[*pjCompressedRow], nSize, jAlpha);
					pwDestRow += nSize;
				}

				++pjCompressedRow;

				nLength -= nSize;

				jFlags = *pjCompressedRow;
				++pjCompressedRow;
				nSize = *(short *) pjCompressedRow;
				pjCompressedRow += sizeof(short);
			} else {
				// This run of pixels is enough.
				if (bFlip) {
					vFillPixelsAlphaRev16(pwDestRow, pwPalette[*pjCompressedRow], nLength, jAlpha);
				} else {
					vFillPixelsAlpha16(pwDestRow, pwPalette[*pjCompressedRow], nLength, jAlpha);
				}
				nLength = 0;
			}
			break;
		default:
			// MessageBox(NULL, "Texture decompression failed!", "Error",
			// MB_OK | MB_TASKMODAL);
			break;
		}
	}
}


#ifdef OS_WINDOWS
int fsize(FILE * file)
{
	int old,
	 r;

	old = ftell(file);
	fseek(file, 0, SEEK_END);
	r = ftell(file);
	fseek(file, old, SEEK_SET);
	return r;
}

#else
#include <unix.h>

int fsize(FILE * file)
{
	int r;

	r = lseek(fileno(file), 0L, SEEK_END);
	lseek(fileno(file), 0L, SEEK_SET);
	return r;
}

#endif // OS_WINDOWS

void Convert2BMPsToTEX(char *inColorFilename, char *inAlphaFilename, char *outFilename, int height)
{
	int i;
	FILE *inColorFile;
	FILE *inAlphaFile;
	FILE *outFile;
	BYTE *inColorData;
	BYTE *inAlphaData;
	int inColorFileSize;
	int inAlphaFileSize;
	int inWidth,
	 inHeight,
	 inWidthInBytes;
	BYTE *inColorPixels;
	BYTE *inAlphaPixels;
	int colorsUsed;
	RGBQUAD inColorPalette[256];

	inColorFile = fopen(inColorFilename, "rb");
	inAlphaFile = fopen(inAlphaFilename, "rb");
	outFile = fopen(outFilename, "wb");
	if ((inColorFile == NULL) ||
		(outFile == NULL)) {
		if (inColorFile)
			fclose(inColorFile);
		if (inAlphaFile)
			fclose(inAlphaFile);
		if (outFile)
			fclose(outFile);
	}
	inColorFileSize = fsize(inColorFile);

	if (inColorFileSize) {
		inColorData = new BYTE[inColorFileSize];
		if (inColorData) {
			FileReadWords(inColorFile, (WORD *) (inColorData + 0));
			FileReadDWords(inColorFile, (DWORD *) (inColorData + 2));
			FileReadWords(inColorFile, (WORD *) (inColorData + 6), 2);
			FileReadDWords(inColorFile, (DWORD *) (inColorData + 10), 4);
			FileReadWords(inColorFile, (WORD *) (inColorData + 26), 2);
			FileReadDWords(inColorFile, (DWORD *) (inColorData + 30), 6);
			FileReadBytes(inColorFile, inColorData + 54, inColorFileSize - 54);
		} else {
			if (inColorFile)
				fclose(inColorFile);
			if (inAlphaFile)
				fclose(inAlphaFile);
			if (outFile)
				fclose(outFile);
			return;
		}
	}
	if (inAlphaFile) {
		inAlphaFileSize = fsize(inAlphaFile);
		if (inAlphaFileSize) {
			inAlphaData = new BYTE[inAlphaFileSize];
			if (inAlphaData) {
				FileReadWords(inAlphaFile, (WORD *) (inAlphaData + 0));
				FileReadDWords(inAlphaFile, (DWORD *) (inAlphaData + 2));
				FileReadWords(inAlphaFile, (WORD *) (inAlphaData + 6), 2);
				FileReadDWords(inAlphaFile, (DWORD *) (inAlphaData + 10), 4);
				FileReadWords(inAlphaFile, (WORD *) (inAlphaData + 26), 2);
				FileReadDWords(inAlphaFile, (DWORD *) (inAlphaData + 30), 6);
				FileReadBytes(inAlphaFile, inAlphaData + 54, inAlphaFileSize - 54);
			} else {
				if (inColorFile)
					fclose(inColorFile);
				if (inAlphaFile)
					fclose(inAlphaFile);
				if (outFile)
					fclose(outFile);
				return;
			}
		} else {
			if (inColorFile)
				fclose(inColorFile);
			if (inAlphaFile)
				fclose(inAlphaFile);
			if (outFile)
				fclose(outFile);
			return;
		}
	}
	inWidth = *(DWORD *) (inColorData + 18);
	inHeight = *(DWORD *) (inColorData + 22);
	inWidthInBytes = inWidth + 3 - ((inWidth - 1) % 4);
	inColorPixels = new BYTE[inWidthInBytes * inHeight];
	memset(inColorPixels, 0, inWidthInBytes * inHeight);

	if (inAlphaFile) {
		inAlphaPixels = new BYTE[inWidthInBytes * inHeight];
		memset(inAlphaPixels, 0, inWidthInBytes * inHeight);
	}
	// Get the palette from the color file.
	colorsUsed = *(DWORD *) (inColorData + 46);

	if (colorsUsed) {
		for (i = 0; i < colorsUsed; ++i) {
			inColorPalette[i].rgbBlue = *(inColorData + 54 + i * 4);
			inColorPalette[i].rgbGreen = *(inColorData + 54 + i * 4 + 1);
			inColorPalette[i].rgbRed = *(inColorData + 54 + i * 4 + 2);
			inColorPalette[i].rgbReserved = *(inColorData + 54 + i * 4 + 3);
		}
		for (i = (int) colorsUsed; i < 256; ++i) {
			inColorPalette[i].rgbBlue = *(inColorData + 54);
			inColorPalette[i].rgbGreen = *(inColorData + 54 + 1);
			inColorPalette[i].rgbRed = *(inColorData + 54 + 2);
			inColorPalette[i].rgbReserved = *(inColorData + 54 + 3);
		}
	} else {
		for (i = 0; i < 256; ++i) {
			inColorPalette[i].rgbBlue = *(inColorData + 54 + i * 4);
			inColorPalette[i].rgbGreen = *(inColorData + 54 + i * 4 + 1);
			inColorPalette[i].rgbRed = *(inColorData + 54 + i * 4 + 2);
			inColorPalette[i].rgbReserved = *(inColorData + 54 + i * 4 + 3);
		}
	}

	// Get the palette from the alpha file.
	// Do later if the palettes aren't in the right order.

	// Check for RLE compression
	switch (*(DWORD *) (inColorData + 30)) {
	case BI_RLE8:
		// Compressed
		BitmapCopyRLEPixels(inWidthInBytes, inColorPixels, inColorData + *(DWORD *) (inColorData + 10));
		break;
	case BI_RGB:
		// Uncompressed
		BitmapCopyPixels(inWidthInBytes, inColorPixels, inColorData + *(DWORD *) (inColorData + 10), inWidthInBytes, inHeight);
		break;
	default:
		// Unknown format, don't use it.
		break;
	}

	if (inAlphaFile) {
		switch (*(DWORD *) (inAlphaData + 30)) {
		case BI_RLE8:
			// Compressed
			BitmapCopyRLEPixels(inWidthInBytes, inAlphaPixels, inAlphaData + *(DWORD *) (inAlphaData + 10));
			break;
		case BI_RGB:
			// Uncompressed
			BitmapCopyPixels(inWidthInBytes, inAlphaPixels, inAlphaData + *(DWORD *) (inAlphaData + 10), inWidthInBytes, inHeight);
			break;
		default:
			// Unknown format, don't use it.
			break;
		}
	}
	WriteTEX(outFile, inColorPixels, inColorPalette, inAlphaPixels, inWidth, inHeight, height, inWidthInBytes);

	// Cleanup
	delete[] inColorPixels;
	delete[] inColorData;
	fclose(inColorFile);
	if (inAlphaFile) {
		delete[] inAlphaPixels;
		delete[] inAlphaData;
		fclose(inAlphaFile);
	}
	fclose(outFile);
}

void WriteTEX(FILE * outFile, BYTE * inColorPixels, RGBQUAD * inColorPalette, BYTE * inAlphaPixels, int inWidth, int inHeight, int height, int inWidthInBytes)
{
	int i,
	 j,
	 k;
	int found;
	int right;
	BYTE c;
	BYTE *aRow;
	int celHeight;
	int numCels;
	BYTE **rows;
	int *lengthOfRows;

	found = FALSE;

	if (height == 0) {
		// Try to calculate height based on a row of black/white
		// alternating.

		i = 0;
		while ((i < inHeight) && !found) {
			aRow = inColorPixels + ((inHeight - i - 1) * inWidthInBytes);
			j = 0;
			right = TRUE;
			while ((j < inWidth) && right) {
				c = aRow[j];
				if (j % 2) {
					if ((inColorPalette[c].rgbRed >= 32) ||
						(inColorPalette[c].rgbGreen >= 32) ||
						(inColorPalette[c].rgbBlue >= 32))
						right = FALSE;
				} else {
					if ((inColorPalette[c].rgbRed <= 224) ||
						(inColorPalette[c].rgbGreen <= 224) ||
						(inColorPalette[c].rgbBlue <= 224))
						right = FALSE;
				}
				++j;
			}
			if (right) {
				found = TRUE;
			} else {
				++i;
			}
		}

		if (found) {
			celHeight = i;
			numCels = (inHeight + 1) / (celHeight + 1);
		} else {
			celHeight = inHeight;
			numCels = 1;
		}
	} else {
		celHeight = height;
		numCels = inHeight / celHeight;
	}

	// Bitmap has been loading into inColorPixels, in uncompressed format.
	// Compress it.
	int h;

	rows = NULL;
	// Pixels from disk were in bottom-up order.
	// Rows will be in top-down order.
	lengthOfRows = new int[numCels * celHeight];
	rows = new PBYTE[numCels * celHeight];

	BYTE *tempRow;
	int foundAlpha;

	// Scan image alpha for an empty alpha channel.
	if (inAlphaPixels) {
		foundAlpha = FALSE;
		for (i = 0; i < numCels; ++i) {
			for (j = 0; j < celHeight; ++j) {
				if (found)
					h = (inHeight - i * (celHeight + 1) - 1) - j;
				else
					h = (inHeight - i * celHeight - 1) - j;
				tempRow = inColorPixels + (h * inWidthInBytes);
				for (k = 0; k < inWidth; ++k) {
					if (tempRow[k] != 0) {
						foundAlpha = TRUE;
						// break out of loops
						k = inWidth;
						j = celHeight;
						i = numCels;
					}
				}
			}
		}
		if (foundAlpha == FALSE) {
			// Don't use the alpha channel, its invalid.
			inAlphaPixels = NULL;
		}
	}
	for (i = 0; i < numCels; ++i) {
		for (j = 0; j < celHeight; ++j) {
			if (found)
				h = (inHeight - i * (celHeight + 1) - 1) - j;
			else
				h = (inHeight - i * celHeight - 1) - j;
			if (inAlphaPixels)
				rows[i * celHeight + j] = TextureCompressRow(inColorPixels + (h * inWidthInBytes), inWidth, lengthOfRows + (i * celHeight + j), inAlphaPixels + (h * inWidthInBytes));
			else
				rows[i * celHeight + j] = TextureCompressRow(inColorPixels + (h * inWidthInBytes), inWidth, lengthOfRows + (i * celHeight + j), 0);
		}
	}

	int tempInt;
	short tempShort;

	// 'tex '
	tempInt = (' ' << 24) | ('x' << 16) | ('e' << 8) | 't';
	FileWriteDWords(outFile, (DWORD *) & tempInt);

	// Version
	tempInt = ('1' << 24) | ('0' << 16) | ('0' << 8) | '0';
	FileWriteDWords(outFile, (DWORD *) & tempInt);

	// palette
	for (i = 0; i < 256; ++i) {
		FileWriteBytes(outFile, (BYTE *) & (inColorPalette[i].rgbRed));
		FileWriteBytes(outFile, (BYTE *) & (inColorPalette[i].rgbGreen));
		FileWriteBytes(outFile, (BYTE *) & (inColorPalette[i].rgbBlue));
	}

	// numLoops
	tempShort = 1;
	FileWriteWords(outFile, (WORD *) & tempShort);

	// numCels
	tempShort = numCels;
	FileWriteWords(outFile, (WORD *) & tempShort);

	// cel data
	for (j = 0; j < numCels; ++j) {
		// width, height, xOrigin, yOrigin, x, y
		tempShort = inWidth;
		FileWriteWords(outFile, (WORD *) & tempShort);
		tempShort = celHeight;
		FileWriteWords(outFile, (WORD *) & tempShort);
		tempShort = 0;
		FileWriteWords(outFile, (WORD *) & tempShort);
		FileWriteWords(outFile, (WORD *) & tempShort);
		FileWriteWords(outFile, (WORD *) & tempShort);
		FileWriteWords(outFile, (WORD *) & tempShort);

		for (i = j * celHeight; i < j * celHeight + celHeight; ++i) {
			tempShort = lengthOfRows[i];
			FileWriteWords(outFile, (WORD *) & tempShort);
			FileWriteBytes(outFile, rows[i], lengthOfRows[i]);
		}
	}

	// Cleanup
	delete[] lengthOfRows;
	for (i = 0; i < numCels * celHeight; ++i)
		delete[] rows[i];
	delete[] rows;
}

#define RLE_ESCAPE  0
#define RLE_EOL     0
#define RLE_EOF     1
#define RLE_JMP     2
#define RLE_RUN     3

void BitmapCopyRLEPixels(WORD wWidthBytes, BYTE * pb, BYTE * prle)
{
	BYTE cnt;
	BYTE b;
	WORD x;
	WORD dx,
	 dy;

	x = 0;

	for (;;) {
		cnt = *prle++;
		b = *prle++;

		if (cnt == RLE_ESCAPE) {
			switch (b) {
			case RLE_EOF:
				return;
			case RLE_EOL:
				pb += wWidthBytes - x;
				x = 0;
				break;
			case RLE_JMP:
				dx = (WORD) * prle++;
				dy = (WORD) * prle++;
				pb += ((DWORD) wWidthBytes * dy) + dx;
				x += dx;
				break;
			default:
				cnt = b;
				x += (WORD) cnt;
				while (cnt-- > 0)
					*pb++ = (*prle++);
				if (b & 1)
					prle++;
				break;
			}
		} else {
			x += (WORD) cnt;

			while (cnt-- > 0)
				*pb++ = (b);
		}
	}
}

void BitmapCopyPixels(WORD, BYTE * pb, BYTE * prle, int wWidthInBytes, int wHeight)
{
	int i,
	 j;

	j = wWidthInBytes * wHeight;
	for (i = 0; i < j; ++i) {
		*pb++ = (*prle++);
	}
}

BYTE PaletteCalculateAlpha(RGBQUAD colors[256], BYTE f, BYTE b, BYTE alpha)
{
	double a;
	BYTE red,
	 green,
	 blue;
	RGBQUAD fColor,
	 bColor;

	a = (double) alpha / 255;
	fColor = colors[f];
	bColor = colors[b];
	red = (BYTE) (bColor.rgbRed - (bColor.rgbRed - fColor.rgbRed) * a);
	green = (BYTE) (bColor.rgbGreen - (bColor.rgbGreen - fColor.rgbGreen) * a);
	blue = (BYTE) (bColor.rgbBlue - (bColor.rgbBlue - fColor.rgbBlue) * a);
	return PaletteFindNearest(colors, red, green, blue);
}

BYTE PaletteFindNearest(RGBQUAD colors[256], BYTE red, BYTE green, BYTE blue)
{
	int i;
	int best;
	int bestDelta;
	int delta;
	int dr,
	 dg,
	 db;
	RGBQUAD *theColor;

	best = 0;
	bestDelta = 256 * 256 * 256;
	theColor = &(colors[0]);
	for (i = 0; i < 256; ++i) {
		dr = theColor->rgbRed - red;
		dg = theColor->rgbGreen - green;
		db = theColor->rgbBlue - blue;
		++theColor;
		delta = abs((int) (dr * dr + dg * dg + db * db));
		if (delta < bestDelta) {
			best = i;
			bestDelta = delta;
		}
	}
	return best;
}

void FileReadBytes(FILE * file, BYTE * buffer, int count)
{
	fread(buffer, 1, count, file);
}

void FileReadWords(FILE * file, WORD * buffer, int count)
{
	fread(buffer, 2, count, file);
	int i;

	for (i = 0; i < count; ++i)
		MacSwapBytes(&(buffer[i]));
}

void FileReadDWords(FILE * file, DWORD * buffer, int count)
{
	fread(buffer, 4, count, file);
	int i;

	for (i = 0; i < count; ++i)
		MacSwapBytes(&(buffer[i]));
}

void FileWriteBytes(FILE * file, BYTE * buffer, int count)
{
	fwrite(buffer, 1, count, file);
}

void FileWriteWords(FILE * file, WORD * buffer, int count)
{
	int i;

	for (i = 0; i < count; ++i)
		MacSwapBytes(&(buffer[i]));
	fwrite(buffer, 2, count, file);
}

void FileWriteDWords(FILE * file, DWORD * buffer, int count)
{
	int i;

	for (i = 0; i < count; ++i)
		MacSwapBytes(&(buffer[i]));
	fwrite(buffer, 4, count, file);
}

void MacSwapBytes(WORD * wData)
{
#ifdef OS_WINDOWS
#else
	SwapBytes(wData);
#endif // OS_WINDOWS
}

void MacSwapBytes(DWORD * dwData) {
#ifdef OS_WINDOWS
#else
	SwapBytes(dwData);
#endif // OS_WINDOWS
}

void SwapBytes(WORD * wData) {
	*wData = (*wData << 8) | (*wData >> 8);
}

void SwapBytes(DWORD * dwData) {
	WORD hiWORD = (WORD) (*dwData >> 16);
	WORD loWORD = (WORD) (*dwData);
	 SwapBytes(&hiWORD);
	 SwapBytes(&loWORD);
	*dwData = loWORD << 16 | hiWORD;
}
