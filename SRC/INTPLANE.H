// INTPLANE.H -- the interface plane
//
// TW

#ifndef _intplane
#define _intplane

#include "sciplane.h"
#include "list.h"
#include "statusln.h"
//#include "scibtngp.h"
#include "intbuttn.h"
#include "command.h" //for MAX_SELECTION_SIZE define
#include "unit.h"

class sciButton;
class Portrait;
class SinglePortrait;
class DualPortrait;
class DrawObj;
class sciCheckBox;
class MapScroller;
class SpellPicker;
//class Unit;
//class sciRadioButton;
//class sciButtonGroup;
//class InterfaceButtonGroup;
//class StatusLine;

typedef enum _PORTRAIT_DISPLAY_ {
	PD_BLANK,		//no portraits displayed
	PD_LIST,		//3 or more local units or 1 or more non-local(using the 'list' moPortraits)
	PD_SINGLE,		//1 local unit only(using mpoSinglePortrait)
	PD_DUAL			//2 local units only
} PORTRAIT_DISPLAY;

class InterfacePlane : public sciPlane {
	
public:
	InterfacePlane();
	~InterfacePlane();
	void Doit();
	void Render (RECT &rUpdateRect);
	Boolean HandleEvent(sciEvent & oEvent);
	void vSetDisplay(DISPLAY_MODE eMode);
	void vShowPortraits(RefList<DrawObj> *poUnitList);
	void vShowButtons();	//shows upgrade and command buttons
	void vShowPicker(Boolean bShowHandToHand);
	void vHidePortraits();
	void vUpdateButtonState(UPGRADE_NAME eUpgrade);
	void vUpdateButtonState(ATTR_CODE eAttribute);
	void vSetDefaultAttackBitmap(RefList<DrawObj> *poUnitList);	//setup the proper bitmap for the button
	void vSetDefaultAttackBitmap(Unit *poUnit);
	Portrait *poOnPortrait();	//returns Portrait object mouse is over a portrait, NULL otherwise
	
	void vUpdateVampireBtnState(Vampire *poVampire);	//refresh button status for a specific vampire
	void vUpdateSelectedVampires();						//refresh button status for all selected vampires

	//Portrait display array functions
	void vAddSelection(DrawObj *poObj);
	void vRemoveSelection(DrawObj *poObj);
	void vClearPortraitArray();

	void vRefreshInventory();
	void vReAdjustScroller();
	void vGameCycle();
	void vUpdateScroller();
	void vZoomMap();

	Unit *poSwapItems(sciEvent &oEvent, Unit *poFromUnit);
	
	//call this function if a unit is vampirized while selected
//	void vChangePortraitToVampire(DrawObj *poFrom, DrawObj *poTo);	
	Bitmap * GetHighlightBitmap();
protected:
	Bitmap * mpoHighlightBitmap;
	Player					*mpoPlayer;			//the local player for this interface
	RefList<DrawObj>		*mpoSelectionList;	//backpointer to current selection list
	Boolean					mbShowButtons;		//T if buttons are shown for local vampires

	Template				*mpoTemplate;		//the interface template for the local player's clan

	void	vButtonStartCoords(int nUpgrade, int &nX, int &nY);	//returns in nX/nY the starting coords of the button

	//aggression buttons
	AttributeButton			moAggrL;
	AttributeButton			moAggrM;
	AttributeButton			moAggrH;
	UpgradeButtonGroup		moAggrGroup;
	void vInitAggression();

	//tenacity buttons
	AttributeButton			moTenaL;
	AttributeButton			moTenaM;
	AttributeButton			moTenaH;
	UpgradeButtonGroup		moTenaGroup;
	void vInitTenacity();

	//level buttons
	List<AttributeButton>	moLevelButtons;
	UpgradeButtonGroup		moLevelGroup;
	void vInitLevel();

	//attack buttons
	List<AttributeButton>	moAttackButtons;
	UpgradeButtonGroup		moAttackGroup;
	void vInitAttack();

	//defend buttons
	List<AttributeButton>	moDefendButtons;
	UpgradeButtonGroup		moDefendGroup;
	void vInitDefend();

	//detect buttons
	List<AttributeButton>	moDetectButtons;
	UpgradeButtonGroup		moDetectGroup;
	void vInitDetect();

	//sight buttons
	List<AttributeButton>	moSightButtons;
	UpgradeButtonGroup		moSightGroup;
	void vInitSight();

	//speed buttons
	List<AttributeButton>	moSpeedButtons;
	UpgradeButtonGroup		moSpeedGroup;
	void vInitSpeed();

	//spell buttons
	List<AttributeButton>	moSpellsButtons;
	UpgradeButtonGroup		moSpellsGroup;
	void vInitSpells();

	//stealth buttons
	List<AttributeButton>	moStealthButtons;
	UpgradeButtonGroup		moStealthGroup;
	void vInitStealth();

	void vHideUpgradeButtons();
	void vShowUpgradeButtons(RefList<DrawObj> *poUnitList);
	void vShowCommandButtons();
	void vHideCommandButtons();

	//command buttons
	sciView					*mpoCommandOverlay;
	CommandButton			moMoveCmd;
	CommandButton			moAttackCmd;
	CommandButton			moFeedCmd;
	CommandButton			moVampirize;
	CommandButton			moTake;
	CommandButton			moCastCmd;
	CommandButton			moGuardCmd;
	CommandButton			moPatrolCmd;
	CommandButton			moScoutCmd;
	void vInitCommandButtons();

	//'Other' command buttons
	CommandButton			moCloakCmd;			//cloak/uncloak toggle
	CommandButton			moDefAttackCmd;		//default attack
	CommandButton			moControlCmd;		//the control panel
	CommandButton			moMapZoomCmd;		//the Map Zoomer

//	RadioDisplayButton		moSpellDisplay;
//	RadioDisplayButton		moUpgradeDisplay;
//	RadioDisplayButton		moSettingsDisplay;
//	sciButtonGroup			moDisplayGroup;
	void vInitOtherButtons();
	DISPLAY_MODE			meDisplayArea;		//the current 'display' (upgrades, spells, control panel, nothing)

	PORTRAIT_DISPLAY mePortraitMode;
	void vGetPortraitMode (RefList<DrawObj> *poUnitList);	//determines what mode portrait area should be in
	List<Portrait>	moPortraits;
	SinglePortrait	*mpoSinglePortrait;
	DualPortrait	*mpoLeftDualPortrait;
	DualPortrait	*mpoRightDualPortrait;
	Boolean			mbPortraitShown;			//True if any portrait is currently displayed
	StatusLine		moStatusLine;

	MapScroller		*mpoMapScroller;
	sciView			*mpoMapOverlay;
	SpellPicker		*mpoPicker;

	DrawObj			*mapoPortraitArray[MAX_SELECTION_SIZE];	//array of pointers for Portrait display positioning
	Boolean bInPortraitArray(DrawObj *poObj);	//returns TRUE of poObj pointer is in array 'mapoPortraitArray'
	int nArrayIndex(DrawObj *poObj);	//returns the array index of poObj in array, -1 if not in array
};

extern InterfacePlane *gpoInterface;

#endif