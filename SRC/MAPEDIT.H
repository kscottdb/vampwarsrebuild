// MAPEDIT.H -- the map editor
//
// TW

#ifndef _mapediting
#define _mapediting

#include "scimain.h"
#include "scievent.h"
#include "mapnode.h"

class sciText;
class DrawObj;
class MapNode;
class Building;
class ObstaclePoint;
class ViewBitmap;
class AddPlane;

const char astrEditModes[][20] = {"None", "Dragging", "Adding", "Origin", "Obstacle"};

typedef enum _EDIT_MODE {
	EM_NONE,
	EM_DRAG,		//dragging around
	EM_ADD,			//adding to map
	EM_ORIGIN,		//changing origin point
	EM_OBSTACLE,	//changing obstacle lists
} EDIT_MODE;

typedef enum _VIEW_TYPE {
	V_EXTERIOR,		// the 'regular' or exterior view of this object
	V_INTERIOR,		// the 'inside' view for this object (right now only buildings have interiors)
} VIEW_TYPE;

class MapEditPlane : public sciPlane {
public:
	//General purpose
	MapEditPlane();
	~MapEditPlane();
	Boolean HandleEvent(sciEvent &evt);
	void Render (RECT &updateRect);
	void Doit();
	void vSetObject(DrawObj *poObj);
	DrawObj *poObject() {return mpoObject;}
	void vSetMode(EDIT_MODE eMode);
	EDIT_MODE eMode(void) {return meMode;}
	void vMarkUpdate (void) {window->Update(mrClip);}

	//clipboard checks
	Boolean bOnClipBoard(void);	//returns TRUE if an object(Unit or DrawObj) is on our mpoMapNode clipboard
	Boolean bOnClipBoard(Building *poBldg); //returns TRUE if building param is on clipboard

	// For obstacle list toggling
	void vClearObstLists(void);					//clears and obstacle lists and readys for input
	void vToggleObstacle(sciEvent oEvent);	//adds or removes obstacle from list
	List<ObstaclePoint> moExtPointList;		//the list of points as they're toggled
	List<ObstaclePoint> moIntPointList;		//the list of points as they're toggled for interiors
	ObstaclePoint *poFindPoint (int nX, int nY, List<ObstaclePoint> *poPointList);	//returns the point if on in given list, or null if off
	VIEW_TYPE eObstacleMode() {return meObstacleMode;}
	void vSetObstMode(VIEW_TYPE eView) {meObstacleMode = eView;}
	Boolean mbListChanged;					//TRUE if obstacle list has been altered in any way
	void vXListScroll(int nHowMuch);		//adds 'nHowMuch' to every X member in moExtPointList and moIntPointList(if present)
	void vYListScroll(int nHowMuch);		//adds 'nHowMuch' to every Y member in moExtPointList and moIntPointList(if present)

	//For adding/dragging
	void vUpdateUnitPositions(Building *poObj);
	Boolean bLegalMove(Building *poObj, sciEvent &oEvent);
	void vSetLists(Building *poBldg);
	MapNode  *mpoMapNode;	//temporary storage(clipboard) for units as we drag and add them to the map
	void vSetUnitDrag (Boolean bDrag) {mbDragUnits = bDrag;}
	Boolean bDragUnits(void) {return mbDragUnits;}
	void vClearUnitIntLists();
	void vMove(DrawObj *poObj);

	// For origin point editing
	void vSetOriginStart(short nX, short nY);
	void vSetCurrent(short nX, short nY);
	short nStartX(void) {return mnStartOriginX;}
	short nStartY(void) {return mnStartOriginY;}

	void vStartNewMap();

protected:
	void vSetText(void);			//sets up text info on left portion of screen

	Boolean			mbDragUnits;	//TRUE if its ok to drag units inside building during the building dragging
	DrawObj			*mpoObject;		//the draw object we want to view
	DRAW_PASS		meLayer;		//the drawing layer we're changing
	EDIT_MODE		meMode;			//the current editing mode we're in for mpoObject
	VIEW_TYPE		meObstacleMode;	//which view we're toggling an obstacle list for(Lobs or Libs)
	sciText			*mpoText1;		//column 1
	sciText			*mpoText2;		//coulmn 2
	sciText			*mpoMousePos;	//info regarding position of mouse
	AddPlane		*mpoAddPlane;	//the plane to add stuff and move it around
	
	//For dragging buildings with units inside.
	//Both of these lists are related closely to one another. Both will always be exactly the same size
	RefList<Unit>	moInteriorUnits;//list of units 'inside' a building for dragging purposes
	List<ObstaclePoint>		moOffsets;		//offsets from building's node the unit(s) are (just like the 'Libs' list)

	//for origin point editing
	short mnStartOriginX;			//the original starting X origin point
	short mnStartOriginY;			//the original starting Y origin point
	short mnCurrentX;				//the current X origin value
	short mnCurrentY;				//the current Y origin value
	ViewBitmap *mpoCrossHair;		//the cross hair bitmap used as a reference

	int				mnLastCel;		//the animation cel mpoObject was last on				
	int				mnLastLoop;		//the loop mpoObject was last on				
	char mstr[256];					//character string

};

extern MapEditPlane *gpoMapEdit;	

#endif
