#include "os.h"
#include "owindow.h"
#include "app.h"
#include "bitmap.h"
#include "palette.h"
#include "sounder.h"
#include "rc.h"

#include "scievent.h"
#include "scigame.h"
#include "sciroom.h"
#include "sciplane.h"
#include "scicursr.h"
#include "game.h"

extern "C" long FAR PASCAL OtherWndProc(HWND hWnd, unsigned message, unsigned wParam, LONG lParam);
extern char *szAppTitle;
extern HINSTANCE hInstance;

BOOL regClass = TRUE;

OtherWindow::OtherWindow()
{
	hWaitCursor = NULL;
	hNormalCursor = NULL;
	hHandCursor = NULL;
	// pBackgroundBitmap = NULL;
	pWorkBitmap = NULL;
}

OtherWindow::~OtherWindow()
{
}

void OtherWindow::MakeWindow()
{
	WNDCLASS wc;
	DWORD dwStyle;

	rcOuter.left = 0;
	rcOuter.top = 0;
	rcOuter.right = 100;
	rcOuter.bottom = 100;
	rcInner = rcOuter;

	// pBackgroundBitmap = NULL;
	pWorkBitmap = new Bitmap((WORD) (rcInner.right - rcInner.left), (WORD) (rcInner.bottom - rcInner.top));

	hWaitCursor = LoadCursor(NULL, IDC_WAIT);
	hNormalCursor = LoadCursor(NULL, IDC_ARROW);
	hHandCursor = LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CURSOR1));
	// wc.style = NULL;
	// wc.style = CS_PARENTDC;
	wc.style = CS_OWNDC | CS_SAVEBITS;
	wc.lpfnWndProc = OtherWndProc;
	wc.lpszClassName = "My Child";
	wc.cbClsExtra = 0;
	wc.cbWndExtra = sizeof(Window far *);
	wc.hInstance = hInstance;
	wc.hIcon = NULL;
	wc.hCursor = hNormalCursor;  
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;

	if (regClass) {
		RegisterClass(&wc);
		regClass = FALSE;
	}
	// dwStyle = WS_CHILDWINDOW;
	dwStyle = WS_POPUP;

	hWnd = CreateWindow(wc.lpszClassName, szAppTitle, dwStyle,
						rcOuter.left, rcOuter.top, rcOuter.right - rcOuter.left, rcOuter.bottom - rcOuter.top,
						pApp->pWindow->hWnd, 0, hInstance, this);

	SetCursor(hNormalCursor);
	// hDC = GetDC(hWnd);
}

void OtherWindow::Show()
{
	Window::Show();
	// if (plane && plane->IsModal())
	// SetActiveWindow(hWnd);
	// else
	SetActiveWindow(pApp->pWindow->hWnd);
}

BOOL bProcessingMouseMove1 = FALSE;

extern "C" LRESULT CALLBACK OtherWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	Window *pWindow;
	POINT point;
	sciEvent event;

	pWindow = (Window *) GetWindowLong(hWnd, 0);

	if (message != WM_CREATE && pWindow == NULL)
		return DefWindowProc(hWnd, message, wParam, lParam);

	switch (message) {
	case WM_CREATE:
		SetWindowLong(hWnd, 0, *(long *) lParam);
		pWindow = (Window *) GetWindowLong(hWnd, 0);
		pWindow->hWnd = hWnd;
		break;
	case WM_PAINT:
		BeginPaint(hWnd, &ps);
		OffsetRect(&(ps.rcPaint), -pWindow->rcInner.left, -pWindow->rcInner.top);
		pWindow->hDC = ps.hdc;
		pWindow->TruePaint(ps.rcPaint);
		pWindow->hDC = 0;
		EndPaint(hWnd, &ps);
		break;
	case WM_ERASEBKGND:
		break;
	case WM_LBUTTONDOWN:
		SetCapture(hWnd);
		point.x = LOWORD(lParam) - pWindow->rcInner.left;
		point.y = HIWORD(lParam) - pWindow->rcInner.top;

		// Now do an SCI event.
		event.type = MOUSE_DOWN;
		event.modifiers = LEFT_MOUSE;
		event.claimed = FALSE;
		event.x = point.x;
		event.y = point.y;
		event.plane = NULL;		// pWindow->plane;
		pWindow->Show();
		pWindow->HandleEvent(event);
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_LBUTTONUP:
		ReleaseCapture();
		pWindow->Show();
		point.x = LOWORD(lParam) - pWindow->rcInner.left;
		point.y = HIWORD(lParam) - pWindow->rcInner.top;

		// Now do an SCI event.
		event.type = MOUSE_UP;
		event.modifiers = LEFT_MOUSE;
		event.claimed = FALSE;
		event.x = point.x;
		event.y = point.y;
		event.plane = NULL;		// pWindow->plane;
		pWindow->HandleEvent(event);
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_RBUTTONDOWN:
		SetCapture(hWnd);
		point.x = LOWORD(lParam) - pWindow->rcInner.left;
		point.y = HIWORD(lParam) - pWindow->rcInner.top;

		// Now do an SCI event.
		event.type = MOUSE_DOWN;
		event.modifiers = RIGHT_MOUSE;
		event.claimed = FALSE;
		event.x = point.x;
		event.y = point.y;
		event.plane = NULL;		// pWindow->plane;
		pWindow->Show();
		pWindow->HandleEvent(event);
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_RBUTTONUP:
		ReleaseCapture();
		pWindow->Show();
		point.x = LOWORD(lParam) - pWindow->rcInner.left;
		point.y = HIWORD(lParam) - pWindow->rcInner.top;

		// Now do an SCI event.
		event.type = MOUSE_UP;
		event.modifiers = RIGHT_MOUSE;
		event.claimed = FALSE;
		event.x = point.x;
		event.y = point.y;
		event.plane = NULL;		// pWindow->plane;
		pWindow->Show();
		pWindow->HandleEvent(event);
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_MOUSEMOVE:
		if (bProcessingMouseMove1)
			break;
		bProcessingMouseMove1 = TRUE;
		point.x = LOWORD(lParam) - pWindow->rcInner.left;
		point.y = HIWORD(lParam) - pWindow->rcInner.top;

		// Now do an SCI event.
		event.type = MOUSE_MOVE;
		event.modifiers = (EVENT_MODIFIER) 0;
		event.claimed = FALSE;
		event.x = point.x;
		event.y = point.y;
		event.plane = NULL;		// pWindow->plane;
		pWindow->HandleEvent(event);

		bProcessingMouseMove1 = FALSE;
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_CHAR:
		// Translate vk key to ascii.
		if (wParam <= 26) {
			point.x = wParam + 0x60;
			point.y = KS_CONTROL;
		} else {
			point.x = wParam;
			point.y = 0;
		}

		// Now do an SCI event.
		event.type = KEY_PRESS;
		event.modifiers = (EVENT_MODIFIER) 0;
		event.claimed = FALSE;
		event.x = 0;
		event.y = 0;
		event.c = (char) wParam;
		event.plane = NULL;		// pWindow->plane;
		pWindow->HandleEvent(event);
		break;
	case WM_KEYDOWN:
		// Translate vk key to ascii.
		if (wParam <= 26) {
			point.x = wParam + 0x60;
			point.y = KS_CONTROL;
		} else {
			point.x = wParam;
			point.y = 0;
		}

		if ((wParam > 27) && (wParam < 41) && (wParam != 32)) {
			// These keys aren't converted to WM_CHAR.
			// Now do an SCI event.
			event.type = KEY_PRESS;
			event.modifiers = (EVENT_MODIFIER) 0;
			event.claimed = FALSE;
			event.x = 0;
			event.y = 0;
			event.c = (char) wParam;
			event.plane = NULL;	// pWindow->plane;
			pWindow->HandleEvent(event);
		}
		break;
	case WM_QUERYNEWPALETTE:
		if (pApp && (pApp->pCurrentPalette)) {
			pApp->pCurrentPalette->ActivatePalette();
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case WM_PALETTECHANGED:
		if (((HWND) wParam != hWnd) && pApp && pApp->pCurrentPalette) {
			pApp->pCurrentPalette->ActivatePalette();
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case WM_NCHITTEST:
		// if (game->curRoom->GetModalPlane() &&
		// (game->curRoom->GetModalPlane() != pWindow->plane))
		// {
		// return HTERROR;
		// }
		// else
		{
			point.x = LOWORD(lParam);
			point.y = HIWORD(lParam);
			ScreenToClient(hWnd, &point);
			if (point.y < 25) {
				return HTCAPTION;
			}
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_CLOSE:
		if (!game->curRoom->GetModalPlane())
			SendMessage(pApp->pWindow->hWnd, WM_CLOSE, 0, 0);
		break;
	case WM_MOVE:
		// if (pWindow->plane)
		// pWindow->plane->Posn(LOWORD(lParam), HIWORD(lParam), FALSE);
		// break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return NULL;
}
