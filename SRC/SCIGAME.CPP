#include "os.h"
#include "scigame.h"
#include "sciscrip.h"
#include "sciroom.h"
#include "scisound.h"
#include "app.h"
#include "inifile.h"
#include "bitmap.h"
#include "resman.h"
#include "timer.h"
#include "sciplane.h"
#include "scicursr.h"
#include "sigs.h"
#include "rooms.h"
#include "sounder.h"
#include "drawobj.h"
#include "objectdb.h"
#include "debug.h"

extern Application *pApp;
extern SIGS *sigs;
extern char *szAppTitle;
extern RefList<sciFeature> unInitedFeatures;
BOOL gameStartup;

sciGame::sciGame()
{
	gameStartup = TRUE;
	quit = FALSE;
	mbPaused = FALSE;
	time = GetTrueTime();
	mnPauseTime = 0;

	nextRoomNum = pApp->pIniFile->ReadValue("Room", ROOM_0);
	prevRoomNum = ROOM_NONE;
	curRoomNum = ROOM_NONE;
	curRoom = NULL;
	script = NULL;
	sound1 = new sciSound();
	sound2 = new sciSound();
	sound2->talkingSound = TRUE;
	mpoDebug = NULL;
}

sciGame::~sciGame()
{
	LoadTheRoom(-1);
	SetScript(NULL);
	if (mpoDebug)
	{
		delete mpoDebug;
	}

	delete sound1;
	delete sound2;
}

Boolean sciGame::HandleEvent(sciEvent & event)
{
	if (curRoom)
		curRoom->HandleEvent(event);
	return event.claimed;
}

int sciGame::GetTime()
{
	return time;
}

int sciGame::GetTrueTime()
{
	return pApp->pTimer->Get60ths();
}

void sciGame::Play()
{
	Init();

	while (!quit) {
		Doit();
	}
}

Boolean sciGame::IsDone()
{
	return quit;
}

void sciGame::QuitGame(Boolean iReallyMeanIt)
{
	if (quit || iReallyMeanIt)
		quit = TRUE;
	else {
		// curRoom->plane->cursor->Hide();
		if (IDYES == MessageBox(NULL, "Are you sure you want to quit?", "Quitting", MB_YESNO | MB_ICONQUESTION | MB_TASKMODAL)) {
			quit = TRUE;
		} else {
			pApp->bDone = FALSE;
		}
		// curRoom->plane->cursor->Show();
	}
}

void sciGame::Init()
{
	//global handler to trap debugging keys
	mpoDebug = new DebugHandler();
	pApp->pWindow->vAddGlobalHandler(mpoDebug);

}

#ifdef _DEBUG
//#define COUNT_DOITS
#endif // _DEBUG

#ifdef COUNT_DOITS
int prevDoitTime = 0;
int curDoitTime = 0;
int numDoits = 0;
#endif // COUNT_DOITS
extern DWORD oldMilli;

void sciGame::Doit()
{
	static int prevDoit = 0;
	static int currDoit = 0;

	currDoit = GetTrueTime();
	if (currDoit == prevDoit) {
		return;
	}

	// if we're paused, the time variable
	// will stop counting
	if (mbPaused) {
		mnPauseTime += (currDoit - prevDoit);
	}
	prevDoit = currDoit;
	oldMilli = timeGetTime();

	time = currDoit - mnPauseTime;

#ifdef COUNT_DOITS
	char buff[64];

	curDoitTime = GetTrueTime();
	++numDoits;
	if (curDoitTime >= prevDoitTime + 60) {
		prevDoitTime = curDoitTime;
		wsprintf(buff, "%d doits.\n", numDoits);
		DebugString(buff);
		numDoits = 0;
	}
#endif // COUNT_DOITS

	// give the subclass a chance to do its thing
	vGameTick();

	if (!mbPaused) {

		if (nextRoomNum != ROOM_NONE) {
			LoadRoom(nextRoomNum);
		}
		if (script) {
			script->Doit();
		}
		if (curRoom) {
			curRoom->Doit();
		}
		if (pApp && pApp->pWindow) {
			pApp->pWindow->Doit();
		}
	} else {
		//we're paused so check for map editing mode
		
		if (gbMapEditing) {
			//we are in map editing mode so allow doits to the various planes
			if (pApp && pApp->pWindow) {
				pApp->pWindow->Doit();
			}
		}

	}


	if (sigs) {
		sigs->Pump();
	}

	pApp->Idle();
	pApp->WaitTillNextTick();
}


void sciGame::SetScript(sciScript * newScript)
{
	if (script)
		delete script;
	script = newScript;
	if (script)
		script->Init(this);
}

void sciGame::SetNextRoom(int newRoomNum)
{
	nextRoomNum = newRoomNum;
}

void sciGame::LoadRoom(int roomNum)
{
	prevRoomNum = curRoomNum;
	curRoomNum = roomNum;
	nextRoomNum = ROOM_NONE;

	LoadTheRoom(curRoomNum);
}

void sciGame::LoadTheRoom(int roomNum)
{
	sciFeature *obj;

	if (curRoom) {
		if (pApp->pSounder)
			pApp->pSounder->EndSound();
		delete curRoom;
		curRoom = NULL;
		while (unInitedFeatures.Size()) {
			obj = unInitedFeatures.At(0);
			unInitedFeatures.Delete(obj);
			delete obj;
		}
	}
	pApp->PurgeResources(0);
	// if ((roomNum != ROOM_POKER_SIGS) && (roomNum !=
	// ROOM_BLACKJACK_SIGS))
	// SetForegroundWindow(pApp->pWindow->hWnd);
}

int sciGame::GetCurrentRoomNum()
{
	return curRoomNum;
}

void sciGame::SetGameName(char *name)
{
	char windowText[64];

	if (name) {
		wsprintf(windowText, "%s - %s", szAppTitle, name);
		SetWindowText(pApp->pWindow->hWnd, windowText);
	} else {
		SetWindowText(pApp->pWindow->hWnd, szAppTitle);
	}
}
