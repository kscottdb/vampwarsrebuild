#ifndef _scimotn
#define _scimotn

#include "sciobj.h"

class sciActor;
class sciScript;

class sciMotion : public sciObject
	{
	public:
		sciMotion(sciScript *newCaller = NULL);
		~sciMotion();
		virtual void Doit();
		virtual void MotionDone();
		void SetTarget(int newTargetX, int newTargetY);
		virtual void Init(sciActor *newClient);

	protected:
		sciActor *client;
		sciScript *caller;
		Boolean inMotion;
		int targetX, targetY;
		float fX, fY;
		float fXDelta, fYDelta;
		int moveCnt;
	};

class sciMoveTo : public sciMotion
	{
	public:
		sciMoveTo(sciScript *newCaller = NULL, int newTargetX = 0, int newTargetY = 0);
		~sciMoveTo();
	};

#endif // _scimotn
