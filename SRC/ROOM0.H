#ifndef _room0
#define _room0

#include "sciroom.h"
#include "sciscrip.h"

class Room0 : public sciRoom
	{
	public:
		Room0();
		~Room0();
		void Init();
		void Doit();
		virtual Boolean HandleEvent(sciEvent &event);
	};

class Room0Script : public sciScript
	{
	public:
		void ChangeState(int newState);
	};

#endif // _room0
