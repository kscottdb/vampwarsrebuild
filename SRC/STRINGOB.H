// stringob.h -- string objects
//
// DJM

#ifndef _STRINGOB_
#define _STRINGOB_

#include <string.h>
#include <assert.h>
#include "object.h"

class String : public Object {
protected:
	char *mpstr;
	long mlSize;

	void vResize(long lNewSize);
	void vRealloc(long lNewSize);

public:
	String(String &str);
	String(const char *pIn);
	String(String *pso);
	String();
	String(long lSize, char c = ' ');
	~String();

	long lGetSize(void) {return mlSize;};
	void vSetString(const char *pstr);
	void vCat(const char *pstr);

	operator char *() {return mpstr;}

	int operator==(const char *pstr)
	{
		assert(pstr && mpstr);
		return (strcmp(mpstr, pstr) == 0);
	}
	int operator!=(const char *pstr)
	{
		assert(pstr && mpstr);
		return (strcmp(mpstr, pstr) != 0);
	}
	int operator>(const char *pstr)
	{
		assert(pstr && mpstr);
		return (strcmp(mpstr, pstr) > 0);
	}
	int operator<(const char *pstr)
	{
		assert(pstr && mpstr);
		return (strcmp(mpstr, pstr) < 0);
	}
	int operator<=(const char *pstr)
	{
		assert(pstr && mpstr);
		return (strcmp(mpstr, pstr) <= 0);
	}
	int operator>=(const char *pstr)
	{
		assert(pstr && mpstr);
		return (strcmp(mpstr, pstr) >= 0);
	}
};

#endif
