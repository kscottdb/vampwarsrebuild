// DEBUG.H -- the global debugger event handler
//
// TW

#ifndef _debughandler
#define _debughandler

#include "scimain.h"
#include "scievent.h"

class sciText;
class DebugMousePlane;

class DebugHandler : public Object {
public:
	DebugHandler();
	Boolean HandleEvent(sciEvent &evt);
protected:
	DebugMousePlane *mpoMousePosPlane;
};

class DebugMousePlane : public sciPlane {
public:
	DebugMousePlane();
	~DebugMousePlane();
	Boolean HandleEvent(sciEvent &evt);
	void Render (RECT &updateRect);
	void Doit();
protected:
	int nDragDiffX;		//for dragging around
	int nDragDiffY;		//for dragging around
	int nNewX;			//for dragging around - the new X position of plane when buttn released
	int nNewY;			//for dragging around - the new X position of plane when buttn released
	Boolean bMouseDown;	//for dragging around
	Boolean bOnMe(sciEvent &oEvent);
	sciText *mpoMousePos;
	char mstr[256];
};

//class MapEditHandler : public Object {
//public:
//	Boolean HandleEvent(sciEvent &evt);
//};

extern Boolean gbToggleObstacleGrid;
extern Boolean gbMapEditing;
extern Boolean gbToggleInteriors;		//for 'seeing' inside buildings
#endif
