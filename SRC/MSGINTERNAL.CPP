#include "os.h"
#include "procs.h"
#include "MsgInternal.h"

MSGINTERNAL_TYPES MsgInternal::getType()
{
	return meType;
}

UINT MsgInternal::nCreateBinary(BYTE *pBinaryCommand, UINT unSize)
{
	assert(pBinaryCommand);

	UINT nSize = 0;

	pBinaryCommand[0] = (BYTE)meType;	 
	pBinaryCommand[1] = (BYTE)mnPlayer;

	switch (meType) {
	case MSGINTERNAL_NOTSET:
		assert(0);  
		nSize = 2;
		break;
	case MSGINTERNAL_QUITGAME:
	case MSGINTERNAL_CHECKSUM:
		nSize = 2;
		break;
	
	}
	return nSize;
}

void MsgInternal::initParms() {
	for (int i=0;i<MAXPARMS;i++)
	{
		mnParms[i] = 0;
		mjParms[i] = NULL;
	}
}

void MsgInternal::deleteParms() {
	for (int i=0;i<MAXPARMS;i++)
	{
		if (mjParms[i]) {
			delete mjParms[i]; 
			mjParms[i] = NULL;
		}
	}
}

MsgInternal::MsgInternal(LPBYTE pjBinary)
{
	initParms();

	meType = (MSGINTERNAL_TYPES)pjBinary[0];
	mnPlayer = (int) pjBinary[1];

	switch (meType)
	{
	case MSGINTERNAL_NOTSET:
		assert(0);
		break;
	case MSGINTERNAL_CHECKSUM:
		break;
	case MSGINTERNAL_QUITGAME:
		break;
	default:
		vDebugOut("Unknown internal message");
		assert(0);
	}
}


