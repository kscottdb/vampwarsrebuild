// ANIMATIONDB.H -- the game animations database
//
// TW

#ifndef _ANIMATIONDB_
#define _ANIMATIONDB_

#include "scimain.h"

class Cycler;

class AnimationDB : public Object {
public:
	AnimationDB();

	void vAddObject(Cycler *poObject);
	void vRemoveObject(Cycler *poObject);
	void vAnimationCycle();

protected:
	RefList<Cycler>		moObjects;	// list of cyclers
	int					mnSize;		//size of list
};

extern AnimationDB goAnimationDB;

#endif
	
