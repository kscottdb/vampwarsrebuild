#include "os.h"
#include "chat.h"
#include "scitext.h"
#include "sciscrip.h"
#include "sigs.h"
#include <stdio.h>

extern SIGS *sigs;
SendChatButton *sendChatButton = NULL;

#define CHAT_TIMEOUT 30
#define CHAT_FONT 12

class SendChatButtonScript : public sciScript
	{
	public:
		void ChangeState(int newState)
			{
			sciScript::ChangeState(newState);
			switch (state)
				{
				case 0: //Setup
					SetSeconds(CHAT_TIMEOUT);
					break;
				case 1:
					((SendChatButton*)client)->RemoveAChatLine();
					--state;
					SetSeconds(CHAT_TIMEOUT);
					break;
				}
			}
	};

SendChatButton::SendChatButton()
//tw	: Button(0)
	{
	int i;

	isUp = FALSE;
	text = new sciText;
	text->SetFont(CHAT_FONT);
	text->SetWidth(420);
	text->SetHeight(22);
	text->Show();
	text->SetPri(999);
	text->Init();
	SetPri(999);

	for (i = 0; i < NUM_CHAT_LINES; ++i)
		{
		chatLines[i] = new sciText;
		chatLines[i]->SetFont(CHAT_FONT);
		chatLines[i]->SetWidth(420);
		chatLines[i]->SetHeight(22);
		chatLines[i]->Show();
		chatLines[i]->SetPri(999);
		chatLines[i]->Init();
		}
	nextChatLine = 0;

	ChatPosn(110, 50);
	SetScript(new SendChatButtonScript);
	}

void SendChatButton::DoLeftClick()
	{
	char textBuffer[160];

	if (isUp)
		{
		if (strlen(text->GetText()+6))
			{
			// Send message.
			if (sigs)
				{
				sprintf_s(textBuffer, "%s: %s",
					sigs->networkPlayerInfo[sigs->playerNum].name, text->GetText()+6);
				sigs->Send(CHAT_MESSAGE, textBuffer, strlen(textBuffer) + 1);
				}
			else
				{
				sprintf_s(textBuffer, "%s", text->GetText()+6);
				}
			AddAChatLine(textBuffer);
			}
		text->SetText("");
		}
	else
		{
		// Start edit box.
		text->SetText("Chat: ");
		}
	isUp = !isUp;
	}

Boolean SendChatButton::HandleEvent(sciEvent &event)
	{
	char textBuffer[70];

	if (!enabled)
		return FALSE;
	switch (event.type)
		{
		case KEY_PRESS:
			if (IsEnabled() && IsNotHidden())
				{
				if ((tolower(event.c) == hotkey) || (tolower(event.c) == hotkey2))
					{
					DoLeftClick();
					event.claimed = TRUE;
					}
				else
					{
					if (isUp)
						{
						switch (event.c)
							{
							case 8:	// backspace
								if (strlen(text->GetText()) > 6)
									{
									sprintf_s(textBuffer, "%s", text->GetText());
									textBuffer[strlen(text->GetText()) - 1] = 0;
									text->SetText(textBuffer);
									}
								break;
							case 27:	// escape
								text->SetText("Chat: ");
								break;
							default:
								// Make sure the buffer doesn't overflow.
								if (strlen(text->GetText()) < sizeof(textBuffer)-2)
									{
									sprintf_s(textBuffer, "%s%c", text->GetText(), event.c);
									text->SetText(textBuffer);
									}
								break;
							}
						event.claimed = TRUE;
						}
					}
				}
			break;
		}
	return event.claimed;
	}

void SendChatButton::AddAChatLine(char *str)
	{
	int i;

	if (nextChatLine == NUM_CHAT_LINES)
		{
		// Scroll the messages up.
		for (i = 0; i < (NUM_CHAT_LINES-1); ++i)
			{
			chatLines[i]->SetText(chatLines[i+1]->GetText());
			}
		--nextChatLine;
		}
	chatLines[nextChatLine]->SetText(str);
	++nextChatLine;
	script->SetSeconds(CHAT_TIMEOUT);
	}

void SendChatButton::RemoveAChatLine()
	{
	int i;

	if (nextChatLine)
		{
		// Scroll the messages up.
		for (i = 0; i < (NUM_CHAT_LINES-1); ++i)
			{
			chatLines[i]->SetText(chatLines[i+1]->GetText());
			}
		chatLines[nextChatLine-1]->SetText("");
		--nextChatLine;
		}
	}

void SendChatButton::ChatPosn(int x, int y)
	{
	int i;

	text->Posn(x, y + 80);
	for (i = 0; i < NUM_CHAT_LINES; ++i)
		{
		chatLines[i]->Posn(x, y + 14*i);
		}
	}

void SendChatButton::ChatSetSize(int width, int height)
	{
	}
