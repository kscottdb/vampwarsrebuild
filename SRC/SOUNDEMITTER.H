#ifndef _SOUNDEMITTER_H_
#define _SOUNDEMITTER_H_


class SoundEmitter : public Object
{
public:
	~SoundEmitter(); 
	virtual void GetPlayPosition(unsigned int * unX, unsigned int *unY);
};

#endif