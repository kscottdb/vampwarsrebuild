// SPELLPICKER.H -- the spell picker (or hand to hand)
//
// TW

#ifndef _spellpicker
#define _spellpicker

#include "sciplane.h"
#include "list.h"
#include "scibuttn.h"
//#include "command.h" //for MAX_SELECTION_SIZE define
//#include "unit.h"

class StatusLine;

//Button mode - spell or hand to hand 
typedef enum _PICK_MODE_ {
	PICK_SPELL,
	PICK_HAND,
	PICK_CANCEL,
} PICK_MODE;



class PickerButton : public sciButton {
	public:
		PickerButton(PICK_MODE	eMode);
		~PickerButton();
		Boolean HandleEvent(sciEvent &oEvent);
		void DoLeftClick();
		void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
		void vSetStatusText(const char *pstrNewText);
		void vSetSpellIndex(int nSpellIndex) {mnSpellIndex = nSpellIndex;}
		int nSpellIndex() {return mnSpellIndex;}
		int nSpellLevel();			//returns the required level of the associated spell
	protected:
		PICK_MODE	meMode;
		int			mnSpellIndex;	//index into game.moSpells for the spell we're selecting
		StatusLine	*mpoStatusLine;
		char		*mpoStatusText;	//the text that will be display on the status line when mouse is over it
};


class SpellPicker : public sciPlane {
	
public:
	SpellPicker(int nPri, Template *poInterfaceTemplate, StatusLine *poStatusLine);
	~SpellPicker();
	Boolean HandleEvent(sciEvent & oEvent);
	Boolean bShowHandToHand() {return mbShowHandToHand;}
	void vShow(Boolean bShowHandToHand);
	void Hide();
	void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
	void vAddSortedButton(PickerButton *poBtn);	//adds button to moSpells and sorts by level of spell
	void vHideAndReturn();	//hides self and returns to normal interface operation

protected:
	RefList<DrawObj>		*mpoSelectionList;	//backpointer to current selection list
	Template				*mpoTemplate;	//backpointer to the interface template
	StatusLine				*mpoStatusLine;
	PickerButton			moHandToHand;
	PickerButton			moCancel;
	List<PickerButton>		moSpells;
	Boolean					mbShowHandToHand;	//TRUE if we clicked default attack button, FALSE if we clicked on cast buttton

	int						mnNumColumns;	//number of spell icons we can fit on a single row
	int						mnColOffset;	//column offset - icon's bitmap width plus spacing pad
	int						mnRowOffset;	//row offset - icons's bitmap height plus spacing pad
	int						mnStartCol;		//starting column for spells
	int						mnStartRow;		//starting row for spells
	int						mnHandToHandX;	//x position for hand to hand icon
	int						mnHandToHandY;	//y position for hand to hand icon
};

#endif